// File: gasnet_exchange_scan.cxx
// Author: Wim Lavrijsen (WLavrijsen@lbl.gov)

// This exchange scan is a search for performance differences in the order of 
// ghost region exchanges in a simple 2D composition.
//
// For simplicity, double-buffering is assumed and local copies/(un)packing
// are omitted for now.
//
// Example (each number denotes a gasnet node):
//    0  1  2  3
//    4  5  6  7             N
//    8  9 10 11           W o E
//   12 13 14 15             S
//
// Scan cycles:
//
//     W N E S    1) baseline: all 'W', 'N', 'E', 'S'
//   W 1 2 2 2    2) all odd 'W', others 'N', 'E', 'S'
//   N 3 1 3 3    3) all odd 'N', others 'W', 'E', 'S'
//   E 4 4 1 4    4) all odd 'E', others 'W', 'N', 'S'
//   S 5 5 5 1    5) all odd 'S', others 'W', 'E', 'N'
//              6-9) 2-5 but for even (?)
//
// odd fill            even fill
//     recv --            recv --
// s   W  N  E  S     s   W  N  E  S
// e W 2E 2S 1W 2N    e W 6E 6S 1W 6N
// n N 3E 3S 3W 1N    n N 7E 7S 7W 1N
// d E 1E 4S 4W 4N    d E 1E 8S 8W 8N
// | S 5E 1S 5W 5N    | S 9E 1S 9W 9N
//
// Example, how to read: collect (send East, recv West) for odd nodes on cycle 1 as in
//  that cycle all nodes send 'W', 'N', 'E', 'S', so nodes will receive West when they
//  are sending East.
//
// -> exchange data, find optimums

#include "wlav_utils/machine.h"                                              

#include <assert.h>
#include <limits.h>
#include <math.h>  
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>

#include <algorithm>
#include <chrono>
#include <iostream>
#include <iomanip> 
#include <fstream>
#include <numeric> 
#include <sstream> 
#include <utility> 
#include <vector>  

//#define GASNET_PAR 1
#include <gasnet.h>

#ifdef SHEPARD_
#ifdef __PLATFORM_COMPILER_GNU_VERSION_STR
#undef __PLATFORM_COMPILER_GNU_VERSION_STR
#endif
#endif

#include <mpi.h>


#define MEASURE_PUT 1

#ifdef MEASURE_PUT
#define GASNET_OPERATION(a1, a2, a3, a4) gasnet_put_nbi((a1), (a2), (a3), (a4))
#else      // measure get
#define GASNET_OPERATION(a1, a2, a3, a4) gasnet_get_nbi((a3), (a1), (a2), (a4))
#endif

#define TIME_SPENT(start, end) (end.tv_sec * 1000000. + end.tv_usec - start.tv_sec*1000000. - start.tv_usec)

#define GASNET_BARRIER \
   gasnet_barrier_notify(0, GASNET_BARRIERFLAG_ANONYMOUS);\
   gasnet_barrier_wait(0, GASNET_BARRIERFLAG_ANONYMOUS);

#define WLAV_ENVVAL(name, defval) \
   size_t name = getenv("WLAV_"#name) ? atoi(getenv("WLAV_"#name)) : defval;

#define WLAV_ENVVAL2(name) \
   if (getenv("WLAV_"#name)) name = atoi(getenv("WLAV_"#name));

const bool VERBOSE = true;

static int ITERATIONS      = 100000;
static int EXPERIMENTS     = 1;

// TODO: can't get mutex to work beyond threads only?
//static gasneti_mutex_t stats_lock = GASNETI_MUTEX_INITIALIZER;

struct Data {
    Data(size_t sz) : size(sz) { posix_memalign(&data, 8, sz); }
    ~Data() { free(data); }

    void fill(bool up) {
        const size_t N = size/sizeof(int);
        for (size_t i = 0; i < N; i++) ((int*)data)[i] = up ? i : N-1-i;
    }

    bool operator==(const Data& other) {
        if (size != other.size)
            return false;
        const size_t N = size/sizeof(int);
        for (size_t i = 0; i < N; i++) {
            if (((int*)data)[i] != ((int*)other.data)[i])
                return false;
        }
        return true;
    }

    void* data;
    size_t size;

private:
    Data(const Data&) = delete;
    Data& operator=(const Data&) = delete;
};

struct Partner {
    Partner(gasnet_node_t nn, gasnet_seginfo_t* segment_infos) : address(nullptr) {
        node = nn;
        if (node != (gasnet_node_t)-1)
            address = (void*)segment_infos[node].addr;
    }
    bool valid() const { return address != nullptr && node != (gasnet_node_t)-1; }
    gasnet_node_t node;
    void* address;
};

uint64_t communicate_with(const Partner& partner, Data& d) {
    GASNET_BARRIER

    std::chrono::steady_clock::duration myclock = std::chrono::steady_clock::duration::zero();
    auto myclock_start = std::chrono::steady_clock::now();
    if (partner.valid()) {
        for (int i = 0; i < ITERATIONS; ++i) {
            GASNET_OPERATION(partner.node, partner.address, d.data, d.size);
        }
        gasnet_wait_syncnbi_all();
    }
    myclock += std::chrono::steady_clock::now() - myclock_start;

    return std::chrono::duration_cast<std::chrono::microseconds>(myclock).count();
}


//
//--
//
int main( int argc, char* argv[] ) {
// see: https://bitbucket.org/berkeleylab/gasnet README for an example code of
// mixing MPI and gasnet

    WLAV_ENVVAL2(ITERATIONS);
    WLAV_ENVVAL2(EXPERIMENTS);

    WLAV_ENVVAL(BLOCKSIZE,        8192)

    gasnet_init(&argc, &argv);

    size_t segment_size = 64*1024*1024; // 64MB
    size_t segment_max = gasnet_getMaxGlobalSegmentSize();
    if (segment_size > segment_max) 
        segment_size = segment_max; // check later whether it's sufficient

    gasnet_attach(NULL, 0, segment_size, 0);

    if (BLOCKSIZE % 8 != 0) {
        std::cerr << "BLOCKSIZE (" << BLOCKSIZE << ") needs to be a multiple of 8 for alignment" << std::endl;
        exit(1);
    }

    Data data{BLOCKSIZE};

    int mynode = gasnet_mynode();
    gasnet_seginfo_t* segment_infos = (gasnet_seginfo_t*)malloc(gasnet_nodes()*sizeof(gasnet_seginfo_t));
    gasnet_getSegmentInfo(segment_infos, gasnet_nodes());
    if (gasnet_nodes()*BLOCKSIZE*4 > segment_infos[mynode].size) {
        std::cerr << "Segment size (" << segment_infos[mynode].size << ") too small to accomodate messages.\n";
        exit(2);
    }

// bookkeeping
    if (mynode == 0) {
        typedef typename std::chrono::steady_clock::period P;
        typedef typename std::ratio_multiply<P, std::mega>::type TT;
        std::cerr << "CLOCK precision: " << std::fixed << double(TT::num)/TT::den << " us\n";

        std::cerr << "BLOCK SIZE: " << BLOCKSIZE << std::endl;
        std::cerr << "Running " << EXPERIMENTS << " experiment of " << ITERATIONS << " each\n";
#ifdef MEASURE_PUT
        std::cerr << "Measuring PUTs\n";
#else
        std::cerr << "Measuring GETs\n";
#endif
    }

    GASNET_BARRIER

    int isMPIinit = 0;
    if (MPI_Initialized(&isMPIinit) != MPI_SUCCESS) { /* test if MPI already init */
        std::cerr << "Error calling MPI_Initialized()\n";
        exit(3);
    }
    if (!isMPIinit /* MPI not init, so do it */ && MPI_Init(&argc, &argv) != MPI_SUCCESS) {
        std::cerr << "Error calling MPI_Init()\n";
        exit(4);
    }

// set affinity and define current CPU (only needed on Shepard: rely on srun on Cori/Edison)
#ifdef SHEPARD_
    wlav::MachineState::state().setAffinity();
// run at peak (NOT good for mxm, but fine on ibv)
    wlav::MachineState::state().setNodeFreq(wlav::highest);
#endif

    GASNET_BARRIER

// actual jobs start here
    std::chrono::steady_clock::duration fulljob = std::chrono::steady_clock::duration::zero();
    auto myjob_start = std::chrono::steady_clock::now();

// prepare
    int imynode = (int)mynode;
    int irowsz = (int)sqrt(gasnet_nodes());
    int imyrow = imynode / irowsz;
    Partner west{gasnet_node_t((imynode-1) / irowsz < imyrow ? -1 : imynode-1), segment_infos};
    Partner north{gasnet_node_t((imynode-irowsz) < 0 ? -1 : mynode-irowsz), segment_infos};
    Partner east{gasnet_node_t((imynode+1) / irowsz > imyrow ? -1 : imynode+1), segment_infos};
    Partner south{gasnet_node_t((imynode+irowsz) >= (int)gasnet_nodes() ? -1 : imynode+irowsz), segment_infos};

    if (VERBOSE) {
        std::cerr << "Neighbors (W, N, E, S) for " << mynode
                  << " " << (int)west.node
                  << " " << (int)north.node
                  << " " << (int)east.node
                  << " " << (int)south.node << std::endl;
    }

    uint64_t completion_times[4][4] = {
        0, 0, 0, 0,
        0, 0, 0, 0,
        0, 0, 0, 0,
        0, 0, 0, 0
    };

    GASNET_BARRIER

// collect measurements
    for (int iexp = 0; iexp < EXPERIMENTS; ++iexp) {
    // cycle 1
        completion_times[0][2] += communicate_with(west, data);
        completion_times[1][3] += communicate_with(north, data);
        completion_times[2][0] += communicate_with(east, data);
        completion_times[3][1] += communicate_with(south, data);

    // cycle 2 & 6
        for (int eo = 0; eo < 2; ++eo) {
            Partner sends[] = {north, east, south};
            int recvs[] {3, 0, 1}; // S, W, N
            for (int i = 0; i < 3; ++i) {
                if ((mynode+eo) % 2)
                    completion_times[0][recvs[i]] += communicate_with(west, data);
                else
                    communicate_with(sends[i], data);
            }
        }

    // cycle 3 & 7
        for (int eo = 0; eo < 2; ++eo) {
            Partner sends[] = {west, east, south};
            int recvs[] {2, 0, 1}; // E, W, N
            for (int i = 0; i < 3; ++i) {
                if ((mynode+eo) % 2)
                    completion_times[1][recvs[i]] += communicate_with(north, data);
                else
                    communicate_with(sends[i], data);
            }
        }

    // cycle 4 & 8
        for (int eo = 0; eo < 2; ++eo) {
            Partner sends[] = {west, north, south};
            int recvs[] {2, 3, 1}; // E, S, N
            for (int i = 0; i < 3; ++i) {
                if ((mynode+eo) % 2)
                    completion_times[2][recvs[i]] += communicate_with(east, data);
                else
                    communicate_with(sends[i], data);
            }
        }

    // cycle 5 & 9
        for (int eo = 0; eo < 2; ++eo) {
            Partner sends[] = {west, east, north};
            int recvs[] {2, 0, 3}; // E, W, S
            for (int i = 0; i < 3; ++i) {
                if ((mynode+eo) % 2)
                    completion_times[3][recvs[i]] += communicate_with(south, data);
                else
                    communicate_with(sends[i], data);
            }
        }

    }

// average
    const double divisor = EXPERIMENTS*ITERATIONS;
    for (int i = 0; i < 4; ++i) {
        for (int j = 0; j < 4; ++j) {
            completion_times[i][j] = uint64_t(round(completion_times[i][j]/divisor));
        }
    }

// stats if desired
    const char* coutput = getenv("WLAV_STATS_FILE_FILE_OUT");
    if (coutput) {
        GASNET_BARRIER;
        //gasneti_mutex_lock(&stats_lock);
        FILE* output = fopen(coutput, "a");
        lockf(fileno(output), F_LOCK, 0);
        fprintf(output, "Results (node %u):\n", gasnet_mynode());
        for (int i = 0; i < 4; ++i) {
            fprintf(output, "%10lu %10lu %10lu %10lu\n",
                   completion_times[i][0], completion_times[i][1],
                   completion_times[i][2], completion_times[i][3]);
        }
        fflush(output);
        lockf(fileno(output), F_ULOCK, 0);
        fclose(output);
        //gasneti_mutex_unlock(&stats_lock);
        GASNET_BARRIER;
    }

    GASNET_BARRIER

// push all results onto node 0's segment and optimize
    if (mynode != 0) {
    // TODO: push results here
        //gasnet_put(0, &zero_segment[(mynode*3  )*bundle_range], injection_times, bundle_range*sizeof(double));
        //gasnet_put(0, &zero_segment[(mynode*3+1)*bundle_range], network_times,   bundle_range*sizeof(double));
        //gasnet_put(0, &zero_segment[(mynode*3+2)*bundle_range], total_times,     bundle_range*sizeof(double));
    }

    GASNET_BARRIER

// done
    fulljob += std::chrono::steady_clock::now() - myjob_start;
    if (mynode == 0)
        std::cerr << "job duration: " << std::chrono::duration_cast<std::chrono::seconds>(fulljob).count() << " seconds\n";

    free(segment_infos);

    MPI_Barrier(MPI_COMM_WORLD);
    if (!isMPIinit) MPI_Finalize();

    gasnet_exit(0);

    return 0;
} 
