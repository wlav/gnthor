// File: gasnet_permute_slow.cxx
// Author: Wim Lavrijsen (WLavrijsen@lbl.gov)

// This is a random (through job placement) search of "global" network
// inefficiencis. It tries permutations on the slowest node and the nodes
// that communicate with it to see whether that has any effect on total
// exchange time (this assumes a barrier at the end of the exchange, so
// that the slowest node is the critical path).
//
// For simplicity, double-buffering is assumed and local copies/(un)packing
// are omitted for now.
//
// Example (each number denotes a gasnet node):
//    0  1  2  3
//    4  5  6  7             N
//    8  9 10 11           W o E
//   12 13 14 15             S
//
// Bench recipe:
//
//  1) run all nodes _individually_ in N, S, E, W order == base line
//    a) permute order for all to find optimum
//  2) run all processes _collectively_ in N, S, E, W order
//  3) find slowest and see whether there could be improvement to base line
//  4) permute slowest while running collectively
//  5) permute neighbors of slowest while running collectively
//

#define MEASURE_PUT 1

static int ITERATIONS      = 10000;
static int EXPERIMENTS     = 10;
const bool VERBOSE         = true;

#include "common.h"


//
//--
//
int main( int argc, char* argv[] ) {
// see: https://bitbucket.org/berkeleylab/gasnet README for an example code of
// mixing MPI and gasnet

    WLAV_ENVVAL2(ITERATIONS);
    WLAV_ENVVAL2(EXPERIMENTS);

    WLAV_ENVVAL(BLOCKSIZE,        8192)

#include "setup.inc"

// collect measurements
    double baseline_time = 0., exchange_time = 0.;

#ifdef MEASURE_BASELINE
// establish baseline
    const char* jobid = getenv("SLURM_JOB_ID");
    std::ostringstream ss;
    ss << jobid << ".lock";
    FILE* joblock = fopen(ss.str().c_str(), "a");
    lockf(fileno(joblock), F_LOCK, 0);
    for (int iexp = 0; iexp < EXPERIMENTS; ++iexp) {
        Partner* partners[] = DEFAULT_EXCHANGE_ORDER;
        baseline_time += communicate_with(partners, data, false);
    }
    lockf(fileno(joblock), F_ULOCK, 0);
    baseline_time /= EXPERIMENTS*ITERATIONS;

    GASNET_BARRIER
#endif // MEASURE_BASELINE

// measure totality
    for (int iexp = 0; iexp < EXPERIMENTS; ++iexp) {
        GASNET_BARRIER
        Partner* partners[] = DEFAULT_EXCHANGE_ORDER;
        exchange_time += communicate_with(partners, data, true);
    }
    exchange_time /= EXPERIMENTS*ITERATIONS;

#ifndef MEASURE_BASELINE
    baseline_time = exchange_time;
#endif

    GASNET_BARRIER

// let node zero report
    double buf[2];
    buf[0] = exchange_time; buf[1] = baseline_time;
    for (int inode = 0; inode < (int)gasnet_nodes(); ++inode) { // all nodes, for finding slowest later
        gasnet_put_nbi_bulk((gasnet_node_t)inode,
            (void*)((double*)segment_infos[inode].addr+2*mynode), buf, 2*sizeof(double));
    }
    gasnet_wait_syncnbi_puts();

    GASNET_BARRIER

    if (mynode == 0) {
        std::cerr << "exchange times each gasnet node (single, all, all/single):\n";
        for (int inode = 0; inode < (int)gasnet_nodes(); ++inode) {
            double te = *((double*)segment_infos[0].addr+2*inode);
            double tb = *((double*)segment_infos[0].addr+2*inode+1);
            std::cerr << inode << ": " << tb << " " << te << " " << te/tb << std::endl;
        }
    }

    GASNET_BARRIER

// now find the slowest
    bool amSlowest = true;
    for (int inode = 0; inode < (int)gasnet_nodes(); ++inode) {
        double t = *((double*)segment_infos[mynode].addr+2*inode);
        if (t > exchange_time) {
            amSlowest = false;
            break;
        }
        // conveniently assume there is no tie
    }

    if (amSlowest)
        std::cerr << mynode << " claims to be slowest: " << exchange_time << std::endl;

    GASNET_BARRIER

// redo full exchange, permuting slowest, check for improvements
    double best_time = exchange_time; std::vector<std::string> best_permute; best_permute.resize(4);
    Partner* perm_partners[] = {&north, &south, &east, &west, nullptr};
    std::sort(perm_partners, perm_partners+4);
    int num_permutes = 0;
    do {
        ++num_permutes;
        double test_time = 0.;
        for (int iexp = 0; iexp < EXPERIMENTS; ++iexp) {
            GASNET_BARRIER

            if (amSlowest) {
                test_time += communicate_with(perm_partners, data, true);
            } else {
                Partner* partners[] = DEFAULT_EXCHANGE_ORDER;
                test_time += communicate_with(partners, data, true);
            }

        }
        test_time /= EXPERIMENTS*ITERATIONS;
        if (test_time < best_time) {
            best_time = test_time;
            for (int i = 0; i < 4; ++i) best_permute[i] = perm_partners[i]->name;
            if (amSlowest) {
                std::cerr << mynode << ": IMPROVED time: " << best_time;
                for (int i = 0; i < 4; ++i) std::cerr << ' ' << best_permute[i];
                std::cerr << std::endl;
            }
        } else if (amSlowest) {
            std::cerr << mynode << ": degraded time: " << test_time;
            for (int i = 0; i < 4; ++i) std::cerr << ' ' << perm_partners[i]->name;
                std::cerr << std::endl;
        }
    } while (std::next_permutation(perm_partners, perm_partners+4));

    assert(num_permutes == 24);

    GASNET_BARRIER;

    if (amSlowest) {
        std::cerr << "best time: " << best_time
                  << " (" << std::setprecision(2) << (1-best_time/exchange_time)*100. << "%)" << std::endl;
        std::cerr << "normal permute: " << DEFAULT_EXCHANGE_ORDER_STRING << std::endl;
        if (best_time < exchange_time) {
            std::cerr << "best permute:  ";
            for (int i = 0; i < 4; ++i) std::cerr << ' ' << best_permute[i];
            std::cerr << std::endl;
        } else
            std::cerr << "permuting showed NO improvement\n";
    }

// done
#include "teardown.inc"

    return 0;
} 
