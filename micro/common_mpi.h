// File: common.h
// Author: Wim Lavrijsen (WLavrijsen@lbl.gov)

// Common classes/functions used in all micro-benches.
//

//#include "wlav_utils/machine.h"                                              

#include <mpi.h>
#include <assert.h>
#include <limits.h>
#include <math.h>  
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>

#include <algorithm>
#include <chrono>
#include <iostream>
#include <iomanip> 
#include <fstream>
#include <numeric> 
#include <sstream> 
#include <utility> 
#include <vector>  



#ifdef SHEPARD_
#ifdef __PLATFORM_COMPILER_GNU_VERSION_STR
#undef __PLATFORM_COMPILER_GNU_VERSION_STR
#endif
#endif

#include <mpi.h>


#define TIME_SPENT(start, end) (end.tv_sec * 1000000. + end.tv_usec - start.tv_sec*1000000. - start.tv_usec)

#define MPI_BARRIER MPI_Barrier(MPI_COMM_WORLD);


#define WLAV_ENVVAL(name, defval) \
   size_t name = getenv("WLAV_"#name) ? atoi(getenv("WLAV_"#name)) : defval;

#define WLAV_ENVVAL2(name) \
   if (getenv("WLAV_"#name)) name = atoi(getenv("WLAV_"#name));

#define DEFAULT_EXCHANGE_ORDER {&north, &south, &east, &west, nullptr}
#define DEFAULT_EXCHANGE_ORDER_STRING "north south east west"

const int NUM_PARTNERS = 4;
const int DIR_STRIDE = 2;

enum class Dir {N=0, E=1, S=2, W=3};


struct Data {
    Data(size_t sz) : size(sz) { posix_memalign(&data, 8, sz); }
    ~Data() { free(data); }

    void fill(bool up) {
        const size_t N = size/sizeof(int);
        for (size_t i = 0; i < N; i++) ((int*)data)[i] = up ? i : N-1-i;
    }

    bool operator==(const Data& other) {
        if (size != other.size)
            return false;
        const size_t N = size/sizeof(int);
        for (size_t i = 0; i < N; i++) {
            if (((int*)data)[i] != ((int*)other.data)[i])
                return false;
        }
        return true;
    }

    bool operator==(int* other) {
        const size_t N = size/sizeof(int);
        for (size_t i = 0; i < N; i++) {
            if (((int*)data)[i] != other[i])
                return false;
        }
        return true;
    }

    void* data;
    size_t size;

private:
    Data(const Data&) = delete;
    Data& operator=(const Data&) = delete;
};

struct Partner {
Partner(const std::string& nm, int nd, char *s_buf, char *r_buf, int stride, int rnode = -1) :
  node(nd), workload(-1.), name(nm), size(stride) {
	      if (node != -1) {
		if(name.compare("west") == 0) {
		  this->s_buf = s_buf + ((int)Dir::W)*stride;
		  this->r_buf = s_buf + ((((int)Dir::W)+DIR_STRIDE)%NUM_PARTNERS)*stride;
		} else if(name.compare("north") == 0) {
		  this->s_buf = s_buf + ((int)Dir::N)*stride;
		  this->r_buf = s_buf + ((((int)Dir::N)+DIR_STRIDE)%NUM_PARTNERS)*stride;
		} else if(name.compare("east") == 0) {
		  this->s_buf = s_buf + ((int)Dir::E)*stride;
		  this->r_buf = s_buf + ((((int)Dir::E)+DIR_STRIDE)%NUM_PARTNERS)*stride;
		} else if(name.compare("south") == 0) {
		  this->s_buf = s_buf + ((int)Dir::S)*stride;
		  this->r_buf = s_buf + ((((int)Dir::S)+DIR_STRIDE)%NUM_PARTNERS)*stride;
		} else { this->s_buf = nullptr; node = -1;}
	      }
	      if(rnode >= 0) r_node = rnode; else r_node = node;
	      
    }
  bool valid() const { return s_buf != nullptr && node != -1; }
  
  int node, r_node;
  void* s_buf, *r_buf;
  size_t size;
  double workload;
  std::string name;
};

extern int myid;

inline double communicate_with(Partner* partners[], Data& d,
        bool use_barriers, double* durations = nullptr,
        bool rev_rec = false)
{
    std::chrono::steady_clock::duration myclock = std::chrono::steady_clock::duration::zero();
    MPI_Request s_request[NUM_PARTNERS],  r_request[NUM_PARTNERS];
    MPI_Status s_stat[NUM_PARTNERS], r_stat[NUM_PARTNERS];
    
    d.fill(myid % 2);

    for (int i = 0; i < ITERATIONS; ++i) {
        if (use_barriers) {
            MPI_BARRIER
	    // having barriers allows verification
            int ip = 0;
	    while (partners[ip]) {
	      Partner* partner = partners[ip];
	      if (partner->valid()) {
		for(int j= 0; j < partner->size/sizeof(int); j++)
		  ((int*)partner->s_buf)[j] = i%3;
	      }
	      ++ip;
	    }
            
            MPI_BARRIER
        }

        auto myclock_start = std::chrono::steady_clock::now();
        
	//TODO: count how many message send by this node, and use this number to replace NUM_PARTNERS in MPI_Waitall. In still not work ,ask them!!!!
	//modify by Xing Pan
	int rev_num=0;
	int send_num=0;
	///////////////

        int ip = 0;
        while (partners[ip]) {
            Partner* partner = partners[ip];
            if (partner->valid()){
	      //MPI_Irecv(partner->r_buf, partner->size, MPI_CHAR, rev_rec ? partner->r_node : partner->node, 1234, MPI_COMM_WORLD, r_request + ip);
              MPI_Irecv(partner->r_buf, partner->size, MPI_CHAR, rev_rec ? partner->r_node : partner->node, 1234, MPI_COMM_WORLD, r_request + rev_num);
	      rev_num++;
	    }
	    ++ip;
        }

	ip = 0;
	while (partners[ip]) {
            Partner* partner = partners[ip];
            if (partner->valid()){
	      //MPI_Isend(partner->s_buf, partner->size, MPI_CHAR, partner->node, 1234, MPI_COMM_WORLD, s_request + ip);
              MPI_Isend(partner->s_buf, partner->size, MPI_CHAR, partner->node, 1234, MPI_COMM_WORLD, s_request + send_num);
	      send_num++;
	    }
	    ++ip;
        }

	//std::cerr << "Begin to waitall\n";

	MPI_Waitall(send_num, s_request, s_stat);
	MPI_Waitall(rev_num, r_request, r_stat);
	//original
	//MPI_Waitall(NUM_PARTNERS, s_request, s_stat);
	//MPI_Waitall(NUM_PARTNERS, r_request, r_stat);



        auto lapsed_time = std::chrono::steady_clock::now() - myclock_start;
        myclock += lapsed_time;
        if (durations)
            durations[i] = std::chrono::duration_cast<std::chrono::microseconds>(lapsed_time).count();
    }

    return std::chrono::duration_cast<std::chrono::microseconds>(myclock).count();
}
