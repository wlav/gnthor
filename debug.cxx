#include "gnthor/config.h"

#define GNTHOR_INTERNAL 1
#include "gnthor/sgasnet.h"
#include "gnthor/debug.h"

size_t GET_MYTH() { return gasnet_mynode(); }
