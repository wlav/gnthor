#define GNTHOR_INTERNAL 1
#include "gnthor/sgasnet.h"
#include "gnthor/callstats.h"

#include <iostream>


gnthor::internal::GNStats gnthor::gnstats;

void gnthor::internal::GNStats::print_stats(int node)
{
    if (node != (int)gasnet_mynode())
        return;

    std::cerr << "GNThor stats for: " << gasnet_mynode()
              << "\n  PUT_NB      (local):   " << num_put_nb_local
              << "\n  PUT_NB      (remote):  " << num_put_nb_remote
              << "\n  PUT_NB_BULK (local):   " << num_put_nb_bulk_local
              << "\n  PUT_NB_BULK (remote):  " << num_put_nb_bulk_remote
              << "\n  GET_NB  (local):       " << num_get_nb_local
              << "\n  GET_NB  (remote):      " << num_get_nb_remote
              << "\n  GET_NB_BULK (local):   " << num_get_nb_bulk_local
              << "\n  GET_NB_BULK (remote):  " << num_get_nb_bulk_remote
              << "\n  WAIT_SYNCNB:           " << num_wait_syncnb
              << "\n  WAIT_SYNCNB_ALL :      " << num_wait_syncnb_all
              << "\n  PUT_NBI (local):       " << num_put_nbi_local
              << "\n  PUT_NBI (remote):      " << num_put_nbi_remote
              << "\n  PUT_NBI_BULK (local):  " << num_put_nbi_bulk_local
              << "\n  PUT_NBI_BULK (remote): " << num_put_nbi_bulk_remote
              << "\n  GET_NBI (local):       " << num_get_nbi_local
              << "\n  GET_NBI (remote):      " << num_get_nbi_remote
              << "\n  GET_NBI_BULK (local):  " << num_get_nbi_bulk_local
              << "\n  GET_NBI_BULK (remote): " << num_get_nbi_bulk_remote
              << "\n  WAIT_SYNCNBI_GETS:     " << num_wait_syncnbi_gets
              << "\n  WAIT_SYNCNBI_PUTS:     " << num_wait_syncnbi_puts
              << "\n  WAIT_SYNCNBI_ALL:      " << num_wait_syncnbi_all
              << "\n  TRY_SYNCNBI_GETS:      " << num_try_syncnbi_gets
              << "\n  TRY_SYNCNBI_PUTS:      " << num_try_syncnbi_puts
              << "\n  TRY_SYNCNBI_ALL:       " << num_try_syncnbi_all
              << "\n  number of bundles:     " << num_bundle_issued
              << " of average size: " << (num_bundle_issued ? tot_bundle_size/num_bundle_issued : 0)
              << std::endl;
}

extern "C" void gnthor_print_stats(int node)
{
    gnthor::gnstats.print_stats(node);
}

extern "C" void gnthor_print_stats_(int* node)
{
    gnthor::gnstats.print_stats(*node);
}
