#ifndef __GNTHOR_REORDERING_H__
#define __GNTHOR_REORDERING_H__

#include "gnthor/datatypes.h"
#include "gnthor/PeerQueue.h"

#include <algorithm>
#include <chrono>
#include <map>
#include <memory>
#include <vector>

#include <float.h>

namespace gnthor {

// individual message posting reordering strategies

class Reordering {
public:
    virtual ~Reordering();

    virtual int Initialize() = 0;
    virtual int Reset() = 0;

    virtual bool Analyze() { return false; }

    virtual size_t issue_request_bundled(
        PeerQueue& queue, issue_policy_t policy, bool drain_queue) = 0;

    virtual size_t issue_bundle(
        PeerQueue& queue, const std::vector<PeerQueue::size_type>& bundle);
    virtual size_t request_completion(PeerQueue& queue);

public:
    static std::shared_ptr<Reordering> get() {
        static std::shared_ptr<Reordering> reordering = get_reordering();
        return reordering;
    }

protected:
    std::vector<PeerQueue::size_type> m_bundle;

private:
    static std::unique_ptr<Reordering> get_reordering();
};


//
// Simply post messages immediately, with no bundling or reordering
//
class Immediate : public Reordering {
public:
    virtual int Initialize() { return 0; }
    virtual int Reset() { return 0; }

    virtual size_t issue_request_bundled(
            PeerQueue& queue, issue_policy_t /* policy */, bool /* drain_queue */) {
        GNTHOR_TRACER(2);

        if (!queue.ready_for_issue())
            return 0;

        queue.get_bundle_for_issue(-1 /* all */, m_bundle);
        return issue_bundle(queue, m_bundle);
    }
};


//
// Post messages with high to low or low to high ordering using latencies or
// sizes, with assigned bundling
//
class Monotonic : public Reordering {
public:
    Monotonic(int bundle_size, bool use_sizes, bool high_to_low) :
	m_bundle_size(bundle_size), m_use_sizes(use_sizes),
	m_high_to_low(high_to_low) {
        m_bundle.resize(m_bundle_size);
    }

public:
    virtual int Initialize();
    virtual int Reset() { return 0; }

    virtual size_t issue_request_bundled(
        PeerQueue& queue, issue_policy_t policy, bool drain_queue);

private:
    int m_bundle_size;
    const bool m_use_sizes;
    const bool m_high_to_low;
};

//
// Post messages with interleaved ordering using latencies or
// sizes, with assigned bundling
//
class Interleave : public Reordering {
public:
    Interleave(int bundle_size, bool use_sizes, bool high_to_low) :
	m_bundle_size(bundle_size), m_use_sizes(use_sizes),
	m_high_to_low(high_to_low) {
        m_bundle.resize(m_bundle_size);
    }

public:
    virtual int Initialize();
    virtual int Reset() { return 0; }

    virtual size_t issue_request_bundled(
        PeerQueue& queue, issue_policy_t policy, bool drain_queue);

private:
    int m_bundle_size;
    const bool m_use_sizes;
    const bool m_high_to_low;
};


//
// Helper class containing all the analysis stats, allowing a per (size, #neighbors)
// grouping of rounds for the global schedule based reordering
//
class AnalysisData {
public:
    AnalysisData(size_t warmup, size_t commsz,
        const std::vector<PeerQueue::size_type>& bundle, PeerQueue& queue) :
            m_skip(warmup), m_best_average(FLT_MAX), m_slowest_node((size_t)-1), m_state(EA_GLOBAL) {

        m_comm_times.reserve(commsz);

        m_order_mapping.resize(bundle.size());
        size_t iorder = 0;
        for (auto ib : bundle) {
            mem_req_t& mr = queue.get_request(ib);
            m_order_mapping[iorder] = std::make_pair(iorder, mr.target);
            iorder += 1;
        }
    }

public:
// current order
    std::vector<std::pair<size_t, gasnet_node_t>> m_order_mapping;

// saved order in case of reversal (could recalculate, but this is
// easier than dealing with nodes that have fewer than max neighbors)
    std::vector<std::pair<size_t, gasnet_node_t>> m_last_order_mapping;

// measurements
    typedef std::chrono::steady_clock clock_t;
    typedef std::vector<float>::size_type count_t;

    std::vector<float> m_comm_times;
    clock_t::time_point m_clock_start;
    count_t m_skip;

// history
    std::vector<std::vector<size_t>> m_orderings;
    std::vector<float> m_sd_times;
    std::vector<gasnet_node_t> m_order;
    float m_best_average;
    size_t m_slowest_node;

// analysis driver (separate b/c all nodes, to synchronize barriers)
    enum AnalysisState { EA_GLOBAL, EA_LOCAL, EA_REMEASURE, EA_STOPPED };
    AnalysisState m_state;
};


//
// Instrument communication phase of fixed domain decomposition and reorder
// based on a global consensus
//
class GlobalSchedule : public Reordering {
    typedef AnalysisData::count_t count_t;
public:
    GlobalSchedule(int interval, int warmup, int remeasure) :
            m_interval((count_t)interval), m_warmup((count_t)warmup),
            m_remeasure((count_t)remeasure) {
    }

public:
    virtual int Initialize();
    virtual int Reset();

    virtual bool Analyze();

    virtual size_t issue_request_bundled(
        PeerQueue& queue, issue_policy_t policy, bool drain_queue);
    virtual size_t request_completion(PeerQueue& queue);

private:
// configuration
    const count_t m_interval, m_warmup, m_remeasure;

// measurements per (sum(size), sum(neighbors))
    typedef std::pair<size_t, size_t> AnalysisKey_t;
    AnalysisKey_t m_cur_key;
    std::map<AnalysisKey_t, AnalysisData> m_analysis_data;
};

} // namespace gnthor

#endif // ! __GNTHOR_REORDERING_H__
