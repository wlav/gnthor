// File: gasnet_exchange_one.cxx
// Author: Wim Lavrijsen (WLavrijsen@lbl.gov)

// Run a single permutation of a 2D exchange with neighbors to test the
// working of reordering in the run-time
//

#define MEASURE_PUT 1

static int ITERATIONS      = 10000;
static int EXPERIMENTS     = 1;
const bool VERBOSE         = true;

#ifdef USE_GNTHOR
#include "gnthor/sgasnet.h"
#endif
#include "common.h"


//
//--
//
int main( int argc, char* argv[] ) {
// see: https://bitbucket.org/berkeleylab/gasnet README for an example code of
// mixing MPI and gasnet

    WLAV_ENVVAL2(ITERATIONS);
    WLAV_ENVVAL2(EXPERIMENTS);

    WLAV_ENVVAL(BLOCKSIZE,        262144)

#include "setup.inc"

// collect measurements for each permutation
    Partner* perm_partners[] = DEFAULT_EXCHANGE_ORDER;
    double* durations = new double[ITERATIONS];
    double exchange_time = 0., variance = 0., skew = 0.;

 // measure totality
    for (int iexp = 0; iexp < EXPERIMENTS; ++iexp) {
       GASNET_BARRIER

       double exp_time = communicate_with(
           perm_partners, data, true, durations, segment_infos);
       exchange_time += exp_time;          // total exchange time all experiments
       exp_time /= ITERATIONS;             // average of current expriment
       double exp_variance = 0., exp_skew = 0.;
       if (VERBOSE && !mynode)
          std::cerr << mynode << ": AVERAGE(" << iexp << "): " << exp_time << std::endl;
       for (int i = 0; i < ITERATIONS; ++i) {
          double d = durations[i] - exp_time;
          exp_variance += d*d;            // variance this experiment only
          exp_skew += d*d*d;              // variance this experiment only
       }

       variance += exp_variance;           // total variance of all experiments
       double sig = sqrt(variance/(ITERATIONS-1));
       skew += exp_skew/pow(sig, 3);
    }
    exchange_time /= EXPERIMENTS*ITERATIONS;
    double stddev = sqrt(variance/(EXPERIMENTS*ITERATIONS-1));
    skew /= EXPERIMENTS*ITERATIONS;
    if (VERBOSE && !mynode)
       std::cerr << mynode << ": STD DEV: " << stddev << " SKEW: " << skew << std::endl;

    GASNET_BARRIER

// let node zero report
    const int NBUF = 3;
    double buf[NBUF];
    buf[0] = exchange_time; buf[1] = stddev; buf[2] = skew;
    for (int inode = 0; inode < (int)gasnet_nodes(); ++inode) { // all nodes, for finding slowest later
       gasnet_put_nbi_bulk((gasnet_node_t)inode,
          (void*)((double*)segment_infos[inode].addr+NBUF*mynode), buf, NBUF*sizeof(double));
    }
    gasnet_wait_syncnbi_puts();

    GASNET_BARRIER

    if (mynode == 0) {
       std::vector<double> all_def_times; all_def_times.reserve(gasnet_nodes());

       std::cerr << "exchange times each gasnet node (average, stddev, stddev/avg, skew):\n";
       for (int inode = 0; inode < (int)gasnet_nodes(); ++inode) {
          double et = *((double*)segment_infos[0].addr+NBUF*inode);
          double sd = *((double*)segment_infos[0].addr+NBUF*inode+1);
          double ft = *((double*)segment_infos[0].addr+NBUF*inode+2);
          std::cerr << inode << ": " << et << " " << sd << " " << sd/et << " " << ft << std::endl;
          all_def_times.push_back(et);
       }

       std::sort(all_def_times.begin(), all_def_times.end());
       std::cerr << "RESULT:";
       for (int i = 0; i < 4; ++i) std::cerr << ' ' << perm_partners[i]->name;
       std::cerr << " " << all_def_times.back() << std::endl;
    }

    GASNET_BARRIER

    delete [] durations;

// done
#include "teardown.inc"

    return 0;
} 
