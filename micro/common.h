// File: common.h
// Author: Wim Lavrijsen (WLavrijsen@lbl.gov)

// Common classes/functions used in all micro-benches.
//

#include "wlav_utils/machine.h"                                              

#include <assert.h>
#include <limits.h>
#include <math.h>  
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>

#include <algorithm>
#include <chrono>
#include <iostream>
#include <iomanip> 
#include <fstream>
#include <numeric> 
#include <sstream> 
#include <utility> 
#include <vector>  

//#define GASNET_PAR 1
#include <gasnet.h>

#ifdef SHEPARD_
#ifdef __PLATFORM_COMPILER_GNU_VERSION_STR
#undef __PLATFORM_COMPILER_GNU_VERSION_STR
#endif
#endif

#include <mpi.h>


#ifdef MEASURE_PUT
#define GASNET_OPERATION(a1, a2, a3, a4) gasnet_put_nbi((a1), (a2), (a3), (a4))
#else      // measure get
#define GASNET_OPERATION(a1, a2, a3, a4) gasnet_get_nbi((a3), (a1), (a2), (a4))
#endif

#define TIME_SPENT(start, end) (end.tv_sec * 1000000. + end.tv_usec - start.tv_sec*1000000. - start.tv_usec)

#define GASNET_BARRIER \
   gasnet_barrier_notify(0, GASNET_BARRIERFLAG_ANONYMOUS);\
   gasnet_barrier_wait(0, GASNET_BARRIERFLAG_ANONYMOUS);

#define WLAV_ENVVAL(name, defval) \
   size_t name = getenv("WLAV_"#name) ? atoi(getenv("WLAV_"#name)) : defval;

#define WLAV_ENVVAL2(name) \
   if (getenv("WLAV_"#name)) name = atoi(getenv("WLAV_"#name));

#define DEFAULT_EXCHANGE_ORDER {&north, &south, &east, &west, nullptr}
#define DEFAULT_EXCHANGE_ORDER_STRING "north south east west"
//#define DEFAULT_EXCHANGE_ORDER {&north, &west, &south, &east, nullptr}
//#define DEFAULT_EXCHANGE_ORDER_STRING "north west south east"

const int NUM_PARTNERS = 4;

struct Data {
    Data(size_t sz) : size(sz) { posix_memalign(&data, 8, sz); }
    ~Data() { free(data); }

    void fill(bool up) {
        const size_t N = size/sizeof(int);
        for (size_t i = 0; i < N; i++) ((int*)data)[i] = up ? i : N-1-i;
    }

    bool operator==(const Data& other) {
        if (size != other.size)
            return false;
        const size_t N = size/sizeof(int);
        for (size_t i = 0; i < N; i++) {
            if (((int*)data)[i] != ((int*)other.data)[i])
                return false;
        }
        return true;
    }

    bool operator==(int* other) {
        const size_t N = size/sizeof(int);
        for (size_t i = 0; i < N; i++) {
            if (((int*)data)[i] != other[i])
                return false;
        }
        return true;
    }

    void* data;
    size_t size;

private:
    Data(const Data&) = delete;
    Data& operator=(const Data&) = delete;
};

struct Partner {
    Partner(const std::string& nm, gasnet_node_t nd, gasnet_seginfo_t* segment_infos) :
            node(nd), address(nullptr), workload(-1.), name(nm) {
        if (node != (gasnet_node_t)-1)
            address = (void*)segment_infos[node].addr;
    }
    bool valid() const { return address != nullptr && node != (gasnet_node_t)-1; }
    void set_workload(gasnet_seginfo_t* segment_infos) {
        if (address) workload = *((double*)segment_infos[gasnet_mynode()].addr+2*node);
    }
    gasnet_node_t node;
    void* address;
    double workload;
    std::string name;
};

inline double communicate_with(Partner* partners[], Data& d,
        bool use_barriers, double* durations = nullptr,
        gasnet_seginfo_t* seg_infos = nullptr)
{
    std::chrono::steady_clock::duration myclock = std::chrono::steady_clock::duration::zero();

    d.fill(gasnet_mynode() % 2);

    for (int i = 0; i < ITERATIONS; ++i) {
        if (use_barriers) {
            GASNET_BARRIER

            if (seg_infos && i != 0) {
            // having barriers allows verification
                int ip = 0;
                while (partners[ip]) {
                    Partner* partner = partners[ip];
                    if (partner->valid()) {
                        int* recv = (int*)((char*)seg_infos[gasnet_mynode()].addr+ \
                                  partner->node*d.size);
                        if (!(d == recv)) {
                            std::cerr << "data verification failed!" << std::endl;
                            exit(1);
                        }
                    }
                    ++ip;
                }
            }

            d.fill(i%3);

            GASNET_BARRIER
        }

        auto myclock_start = std::chrono::steady_clock::now();

        int ip = 0;
        while (partners[ip]) {
            Partner* partner = partners[ip];
            if (partner->valid())
                GASNET_OPERATION(partner->node,
                                 (char*)partner->address+gasnet_mynode()*d.size,
                                 d.data, d.size);
            ++ip;
        }
#ifdef MEASURE_PUT
        gasnet_wait_syncnbi_puts();
#else
        gasnet_wait_syncnbi_gets();
#endif

        auto lapsed_time = std::chrono::steady_clock::now() - myclock_start;
        myclock += lapsed_time;
        if (durations)
            durations[i] = std::chrono::duration_cast<std::chrono::microseconds>(lapsed_time).count();
    }

    return std::chrono::duration_cast<std::chrono::microseconds>(myclock).count();
}
