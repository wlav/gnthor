#ifndef __GNTHOR_DATATYPES_H__
#define __GNTHOR_DATATYPES_H__

// available request types
typedef enum {q_memget       =241, q_memput       =365,
              q_memget_i     =242, q_memput_i     =366,
              q_memget_bulk  =243, q_memput_bulk  =367,
              q_memget_i_bulk=244, q_memput_i_bulk=368 } req_type_t;

// possible request states
typedef enum {r_completed=123, r_pending=456, r_issued=789} req_state_t;

// issue policy for draining queue
typedef enum {all, only_aged, fixed_count, credit, timed} issue_policy_t;


// individual request
typedef struct mem_req_struct {
   gasnet_handle_t handle;

   void*         destination;
   void*         source;
   size_t        size;
   gasnet_node_t target;

   req_type_t           type;        // put or get
   volatile req_state_t state;       // processing state of request

   char padding[24];                 // rounds to 64 bytes (TODO: verify)
} mem_req_t;

#endif // ! __GNTHOR_DATATYPES_H__
