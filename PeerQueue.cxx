#include "gnthor/config.h"

#define GNTHOR_INTERNAL 1
#include "gnthor/debug.h"
#include "gnthor/internal.h"

#include "gnthor/PeerQueue.h"
#include "gnthor/Reordering.h"

#include <assert.h>


//
// Queue message request. Both gets and puts are entered in the same queue (just
// distinguished by request type).
//
gasnet_handle_t gnthor::PeerQueue::enqueue_request(req_type_t type,
        gasnet_node_t target, void* src, void* dest, size_t nbytes)
{
    GNTHOR_TRACER(2);
    uint64_t slot = (m_head+1) % REQ_QUEUE_LENGTH;
    mem_req_t& cur_request = m_requests[slot];

    if (cur_request.state && cur_request.state != r_completed) {
    // slot belongs to an outstanding message: clear before moving on
        GNTHOR_TRACER(5);
        if (Reordering::get()->issue_request_bundled(*this, fixed_count, false /* not drain_queue */))
            Reordering::get()->request_completion(*this);
        if (cur_request.state != r_completed) {
        // current reordering is waiting on a bundle, so force queue drain
        // TODO: be smarter about this and only clear the slot we need
            GNTHOR_TRACER(5);
            gnthor::internal::drain_pending_requests();

            if (cur_request.state != r_completed) {
                std::cerr << " No slot available and failed to drain queue: "
                          << cur_request.state << std::endl;
                exit(3);
            }
        }
    }

// take over cur_request
    m_head++;

    cur_request.destination = dest;
    cur_request.source      = src;
    cur_request.size        = nbytes;
    cur_request.type        = type;
    cur_request.state       = r_pending;
    cur_request.target      = target;

// TODO: a lot of this stuff is for pthreads (i.e. later)
    __sync_synchronize();

// should only get here for off-node requests
    assert(GNTHOR_CAPTURE_LOCAL || !gnthor::internal::is_same_node(target));
    gasnet_handle_t handle =
        internal::synthesize_handle(GASNET_INVALID_HANDLE, slot, false /* off-node */);
    return handle;
}
