#ifndef __GNTHOR_DEBUG_H__
#define __GNTHOR_DEBUG_H__

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif // ifdef __cplusplus
    size_t GET_MYTH();
#ifdef __cplusplus
}
#endif // ifdef __cplusplus

#define VERBOSE_LEVEL 0

#ifdef VERBOSE_LEVEL
#include <iostream>
#define GNTHOR_TRACER(level) do { if ((level <= VERBOSE_LEVEL)) { std::cerr << level << ": [" << GET_MYTH() << "] " << __FUNCTION__ << ": " << __FILE__ << " " << __LINE__ << std::endl; } } while(0);
#else
#define GNTHOR_TRACER(level)
#endif

extern bool GNTHOR_VERBOSE;

#endif // !__GNTHOR_DEBUG_H__
