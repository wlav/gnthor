#include "gnthor/config.h"

#define GNTHOR_INTERNAL 1
#include "gnthor/debug.h"
#include "gnthor/internal.h"

#include "gnthor/PeerQueue.h"
#include "gnthor/Reordering.h"

#include <cmath>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#ifndef HOST_NAME_MAX
#if defined(__APPLE__)
#define HOST_NAME_MAX 255
#else
#define HOST_NAME_MAX 64
#endif /* __APPLE__ */
#endif /* HOST_NAME_MAX */

// single peer queue as the server is inline (1 queue per gasnet node)
gnthor::PeerQueue g_myqueue;

// peer queue for local messages
gnthor::PeerQueue g_myqueue_aux;

// run-time state
gnthor::internal::EState gnthor::internal::g_state = gnthor::internal::EO_ANALYSIS_ON;
gnthor::internal::EIssuePolicy gnthor::internal::g_issue_policy = gnthor::internal::EI_CONTINUOUS_ISSUE;
bool gnthor::internal::g_analysis_ready = false;


//
// GasnetNodeMap initialization
//
gnthor::internal::GasnetNodeMap::GasnetNodeMap() : m_info(0) {
// basic counts
    gasnet_node_t nnodes = gasnet_nodes();
    gasnet_node_t mynode = gasnet_mynode();

    m_info = (gasnet_nodeinfo_t*)malloc(sizeof(gasnet_nodeinfo_t)*nnodes);
    gasnet_getNodeInfo(m_info, nnodes);

    m_myhost = m_info[mynode].host;

    m_ranks_this_node = 0;
    for (size_t i = 0; i < nnodes; ++i) {
        if (m_info[i].host == m_myhost) ++m_ranks_this_node;
    }

// handling a 'left-over' node should not be too hard, but
// make sure we are aware ...
    if (nnodes % m_ranks_this_node != 0) {
        fprintf(stderr,
            "no equal division of ranks over nodes (%u %% %zd = %zd)\n",
            nnodes, m_ranks_this_node, nnodes % m_ranks_this_node);
    }

// node count
    m_physical_nodes = (size_t)ceil((double)nnodes/m_ranks_this_node);
    if (nnodes % m_ranks_this_node && m_myhost/m_ranks_this_node == m_physical_nodes-1) {
    // only wrong if we're on the last node: use the count of thread 0
    // to find the correct number of nodes
        m_ranks_per_node = 0;
        gasnet_node_t host_0 = m_info->host;
        for (size_t i = 0; i < nnodes; ++i) {
            if (m_info[i].host == host_0) ++m_ranks_per_node;
    }
        m_physical_nodes = (size_t)ceil((double)nnodes/m_ranks_per_node);
    } else
        m_ranks_per_node = m_ranks_this_node;
#ifdef CEREBRO_
    m_physical_nodes *= 2;    // for debugging
    m_ranks_per_node /= 2;    // id.
    assert(m_physical_nodes == 2);
#endif
}

//
// Helper to initialize host names late (this requires an all-to-all, so
// gasnet needs to have been attached).
//
void gnthor::internal::GasnetNodeMap::initialize_hostnames()
{
    gasnet_node_t nnodes = gasnet_nodes();
    gasnet_node_t mynode = gasnet_mynode();

    char hn[HOST_NAME_MAX];
    if (gethostname(hn, HOST_NAME_MAX) != 0)
        strncpy(hn, "<unknown>", HOST_NAME_MAX);
    size_t szname = strlen(hn);

    gasnet_seginfo_t* segment_infos = new gasnet_seginfo_t[nnodes];
    gasnet_getSegmentInfo(segment_infos, nnodes);

    size_t offset = mynode*HOST_NAME_MAX;
    for (size_t i = 0; i < nnodes; ++i) {
        gasnet_put_nbi_bulk(i, (char*)segment_infos[i].addr+offset, hn, szname+1);
    }
    gasnet_wait_syncnbi_puts();
    GASNET_BARRIER;

    m_hostnames.reserve(nnodes);
    for (size_t i = 0; i < nnodes; ++i)
        m_hostnames.push_back((char*)segment_infos[mynode].addr+i*HOST_NAME_MAX);

    delete[] segment_infos;
}

//
// Internalize non-blocking puts/gets and return a faked handle.
//
gasnet_handle_t gnthor::internal::enqueue_request(req_type_t type,
        gasnet_node_t target, void* src, void*  dest, size_t nbytes)
{
    GNTHOR_TRACER(3);
    gasnet_handle_t handle = g_myqueue.enqueue_request(type, target, src, dest, nbytes);
    if (g_issue_policy == EI_CONTINUOUS_ISSUE)
        Reordering::get()->issue_request_bundled(g_myqueue, fixed_count, false /* drain_queue */);
    return handle;
}

//
// issue all requests pending in the queue
//
size_t gnthor::internal::issue_pending_requests()
{
    GNTHOR_TRACER(1);
    return Reordering::get()->issue_request_bundled(g_myqueue, fixed_count, true);
}

//
// complete all requests pending in the queue
//
size_t gnthor::internal::complete_pending_requests() {
    GNTHOR_TRACER(1);
    return Reordering::get()->request_completion(g_myqueue);
}
