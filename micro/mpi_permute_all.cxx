// File: gasnet_permute_all.cxx
// Author: Wim Lavrijsen (WLavrijsen@lbl.gov)

// Run all permutations of a 2D exchange with neighbors to see whether any
// order is faster than any others.
//

#define MEASURE_PUT 1


static int ITERATIONS      = 10000;
static int EXPERIMENTS     = 1;
const bool VERBOSE         = true;

#ifdef USE_GNTHOR
#include "gnthor/sgasnet.h"
#include "gnthor/gnthor.h"
#endif
#include "common_mpi.h"

int myid, numprocs;

//
//--
//
int main( int argc, char* argv[] ) {

  WLAV_ENVVAL2(ITERATIONS);
  WLAV_ENVVAL2(EXPERIMENTS);
  WLAV_ENVVAL(BLOCKSIZE,        262144);
  //add by Xing Pan to record the repeat measurement times.
  WLAV_ENVVAL(MEASUREMENTS,        1);

  //std::cerr << "BLOCKSIZE is "<< BLOCKSIZE << "\n";
  
  int size, i, j;
  char *s_buf, *r_buf;
  
  int isMPIinit = 0;
  if (MPI_Initialized(&isMPIinit) != MPI_SUCCESS) { /* test if MPI already init */
    std::cerr << "Error calling MPI_Initialized()\n";
    exit(3);
  }
  if (!isMPIinit /* MPI not init, so do it */ && MPI_Init(&argc, &argv) != MPI_SUCCESS) {
    std::cerr << "Error calling MPI_Init()\n";
    exit(4);
  }
  MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
  MPI_Comm_rank(MPI_COMM_WORLD, &myid);

  //allocate some memory
  unsigned long align_size = sysconf(_SC_PAGESIZE);
   if (BLOCKSIZE % 8 != 0) {
    std::cerr << "BLOCKSIZE (" << BLOCKSIZE << ") needs to be a multiple of 8 for alignment" << std::endl;
    exit(1);
  }
  
  if (posix_memalign((void**)&s_buf, align_size, BLOCKSIZE*NUM_PARTNERS)) {
    fprintf(stderr, "Error allocating host memory\n");
    return 1;
  }
  
  if (posix_memalign((void**)&r_buf, align_size, BLOCKSIZE*NUM_PARTNERS)) {
    fprintf(stderr, "Error allocating host memory\n");
    return 1;
  }
  
  
  Data data{BLOCKSIZE};
    
  int mynode = myid;
   

// bookkeeping
    if (mynode == 0) {
        typedef typename std::chrono::steady_clock::period P;
        typedef typename std::ratio_multiply<P, std::mega>::type TT;
        std::cerr << "CLOCK precision: " << std::fixed << double(TT::num)/TT::den << " us\n";

        std::cerr << "BLOCK SIZE: " << BLOCKSIZE << std::endl;
        std::cerr << "Running " << EXPERIMENTS << " experiment of " << ITERATIONS << " each\n";
#ifdef MEASURE_PUT
        std::cerr << "Measuring PUTs\n";
#else
        std::cerr << "Measuring GETs\n";
#endif
    }

    MPI_BARRIER
      
     
// set affinity and define current CPU (only needed on Shepard: rely on srun on Cori/Edison)
#ifdef SHEPARD_
    wlav::MachineState::state().setAffinity();
// run at peak (NOT good for mxm, but fine on ibv)
    wlav::MachineState::state().setNodeFreq(wlav::highest);
#endif

    MPI_BARRIER

// for simplicity, demand square
    int isr = int(sqrt((double)numprocs));
    if (isr*isr != numprocs) {
       std::cerr << "Require total ranks to be a square ... exiting\n";
       exit(3);
    }

    //std::cerr << "Begin to prepare\n";
// prepare
    int imynode = (int)mynode;
    int irowsz = (int)sqrt(numprocs);
    int imyrow = imynode / irowsz;
    int pw =  (imynode-1) / irowsz < imyrow ? -1 : imynode-1;
    int pn = (imynode-irowsz) < 0 ? -1 : imynode-irowsz;
    int pe = (imynode+1) / irowsz > imyrow ? -1 : imynode+1;
    int ps =  (imynode+irowsz) >= numprocs ? -1 : imynode+irowsz;
    
    Partner west{"west", pw, s_buf, r_buf, BLOCKSIZE, pe};
    Partner north{"north", pn, s_buf, r_buf, BLOCKSIZE, ps};
    Partner east{"east", pe, s_buf, r_buf, BLOCKSIZE, pw};
    Partner south{"south", ps, s_buf, r_buf, BLOCKSIZE, pn};

    if (VERBOSE) {
        std::ostringstream msg;
        msg  << "Neighbors (W, N, E, S) for " << mynode
             << ' ' << (int)west.node
             << ' ' << (int)north.node
             << ' ' << (int)east.node
             << ' ' << (int)south.node << '\n';
        std::cerr << msg.str() << std::flush;
    }

    MPI_BARRIER

    //std::cerr << "Begin to start\n";

// actual jobs start here
    std::chrono::steady_clock::duration fulljob = std::chrono::steady_clock::duration::zero();
    auto myjob_start = std::chrono::steady_clock::now();



      
// collect measurements for each permutation
    Partner* perm_partners[] = DEFAULT_EXCHANGE_ORDER;
    std::sort(perm_partners, perm_partners+4);
    int num_permutes = 0;
    double* durations = new double[ITERATIONS];

    do {
        ++num_permutes;
        double exchange_time = 0., variance = 0., skew = 0., fat_tail = 0.;

#ifdef USE_GNTHOR
        gnthor_start_comm_region();
        gnthor_reset();
#endif

    // measure totality
        for (int iexp = 0; iexp < EXPERIMENTS; ++iexp) {
            MPI_BARRIER

            //double exp_time = communicate_with(perm_partners, data, true, durations);  //non-flipping
            double exp_time = communicate_with(perm_partners, data, true, durations, true); //flipping
            exchange_time += exp_time;          // total exchange time all experiments
            exp_time /= ITERATIONS;             // average of current expriment
            double exp_variance = 0., exp_skew = 0.;
            for (int i = 0; i < ITERATIONS; ++i) {
                double d = durations[i] - exp_time;
                exp_variance += d*d;            // variance this experiment only
                exp_skew += d*d*d;              // variance this experiment only
            }

            double sd_off = exp_time + 2*sqrt(exp_variance/(ITERATIONS-1));
            for (int i = 0; i < ITERATIONS; ++i) {
                if (durations[i] > sd_off) fat_tail += 1.; // tail events in this experiment
            }

            variance += exp_variance;           // total variance of all experiments
            double sig = sqrt(variance/(ITERATIONS-1));
            skew += exp_skew/pow(sig, 3);

        }
        exchange_time /= EXPERIMENTS*ITERATIONS;
        double stddev = sqrt(variance/(EXPERIMENTS*ITERATIONS-1));
        skew /= EXPERIMENTS*ITERATIONS;

        MPI_BARRIER

#ifdef USE_GNTHOR
        gnthor_end_comm_region();
#endif

	//std::cerr << "Begin to report for Measure_Micro\n";

    // let node zero report
        const int NBUF = 4;
        double buf[NBUF];
        buf[0] = exchange_time; buf[1] = stddev; buf[2] = skew; buf[3] = fat_tail;
	double *summa = (double*)malloc(numprocs*NBUF*sizeof(double));
	MPI_Gather(buf, NBUF*sizeof(double), MPI_CHAR, summa, NBUF*sizeof(double), MPI_CHAR, 0, MPI_COMM_WORLD);
               
        if (mynode == 0) {
            std::vector<double> all_def_times; all_def_times.reserve(numprocs);

            std::cerr << "exchange times each gasnet node (average, stddev, fat tail, skew):\n";
            for (int inode = 0; inode < (int)numprocs; ++inode) {
                double et = *((double*)summa+NBUF*inode);
                double sd = *((double*)summa+NBUF*inode+1);
                double sw = *((double*)summa+NBUF*inode+2);
                double ft = *((double*)summa+NBUF*inode+3);
                std::cerr << inode << ": " << et << " " << sd << " " << ft << " " << sw << std::endl;
                all_def_times.push_back(et);
            }

            std::sort(all_def_times.begin(), all_def_times.end());
            //std::cerr << "MEASUREMENTS "<< MEASUREMENTS<<" RESULT"<<"of BLOCKSIZE("<<BLOCKSIZE<<"):";
	    std::cerr << "RESULT";
            for (int i = 0; i < 4; ++i) std::cerr << ' ' << perm_partners[i]->name;
            std::cerr << " " << all_def_times.back() << std::endl;
        }

        MPI_BARRIER

    } while (std::next_permutation(perm_partners, perm_partners+4));

    assert(num_permutes == 24);

    delete [] durations;

// done
//#include "teardown.inc"
    MPI_Finalize();
    return 0;
} 
