#ifndef __GNTHOR_INTERNAL_H__
#define __GNTHOR_INTERNAL_H__

#include <string>
#include <vector>

#include <assert.h>

#define GNTHOR_INTERNAL 1
#include "gnthor/sgasnet.h"
#include "gnthor/debug.h"
#include "gnthor/datatypes.h"
#include "gnthor/PeerQueue.h"

#define GASNET_BARRIER \
    gasnet_barrier_notify(0, GASNET_BARRIERFLAG_ANONYMOUS);\
    gasnet_barrier_wait(0, GASNET_BARRIERFLAG_ANONYMOUS)


// global setting whether to capture local traffic or not (default: false)
extern bool GNTHOR_CAPTURE_LOCAL;

// global setting whether to hide reorder analysis in barriers and whether to use keys
extern bool GNTHOR_ANALYSIS_IN_BARRIER;
extern bool GNTHOR_KEYED_ANALYSIS;

// single peer queue as the server is inline (1 queue per gasnet node)
extern gnthor::PeerQueue g_myqueue;

// auxiliary queue for handles outstanding on the local node
extern gnthor::PeerQueue g_myqueue_aux;


namespace gnthor {
namespace internal {

// state to allow message capture and reordering to be API-driven; this is needed in
// case the application has significant computation to overlap with communication
enum EState { EO_ANALYSIS_OFF, EO_ANALYSIS_ON, EO_COMPLETION_NEEDED };
extern EState g_state;

// TODO: mix this state in and provide a proper state machine
extern bool g_analysis_ready;

// policy (to be set by reordering) for calling to issue after enqueueing
enum EIssuePolicy { EI_CONTINUOUS_ISSUE, EI_DRAIN_ONLY };
extern EIssuePolicy g_issue_policy;


//
// node distribution information
//
class GasnetNodeMap {
public:
    static GasnetNodeMap* get() {
        static GasnetNodeMap gnmap{};
        return &gnmap;
    }

public:
    bool is_same_node(gasnet_node_t node) {
        return m_myhost == m_info[node].host;
    }

    size_t physical_nodes() { return m_physical_nodes; }
    size_t ranks_per_node() { return m_ranks_per_node; }

    const std::string& hostname(gasnet_node_t node) {
       if (m_hostnames.empty()) initialize_hostnames();
       return m_hostnames[node];
    }
    size_t my_host() { return m_myhost; }

private:
    GasnetNodeMap();
    ~GasnetNodeMap() { free(m_info); }

    void initialize_hostnames();

private:
    std::vector<std::string> m_hostnames;
    gasnet_nodeinfo_t* m_info;
    gasnet_node_t      m_myhost;
    size_t  m_ranks_this_node;
    size_t  m_ranks_per_node;
    size_t  m_physical_nodes;
};

//
// helper to map queue indices to 'handles'
//
inline gasnet_handle_t synthesize_handle(gasnet_handle_t /* handle */,
                                         int64_t index, bool on_node)
{
// simply return index as "handle"; assume outside bookkeeping to track the
// right queue
    index = index + 1;      // prevents 0 (== GASNET_INVALID_HANDLE)
    return (gasnet_handle_t)(on_node ? -index : index);
}

//
// internalize get/put requests
//
gasnet_handle_t enqueue_request(req_type_t type,
    gasnet_node_t target, void* src, void* dest, size_t nbytes);

//
// issue, complete, or drain the queue of all pending messages
//
size_t issue_pending_requests();
size_t complete_pending_requests();
inline void drain_pending_requests() {
#ifndef NDEBUG
    EState old_state = g_state;
#endif
    if (issue_pending_requests() || g_state == EO_COMPLETION_NEEDED)
        complete_pending_requests();
#ifndef NDEBUG
    assert(g_state == EO_ANALYSIS_ON || g_state == EO_ANALYSIS_OFF);
    if (old_state == EO_COMPLETION_NEEDED)
        assert(g_state == EO_ANALYSIS_OFF);
#endif
}

//
// helper to define if a gasnet node sits on the same supernode (i.e. same
// machine)
//
inline bool is_same_node(gasnet_node_t node) {
    return GasnetNodeMap::get()->is_same_node(node);
}

inline const std::string& get_hostname(gasnet_node_t node) {
    return GasnetNodeMap::get()->hostname(node);
}

//
// helpers for layout informatiom
//
inline size_t physical_nodes() {
    return GasnetNodeMap::get()->physical_nodes();
}

inline size_t ranks_per_node() {
    return GasnetNodeMap::get()->ranks_per_node();
}

inline size_t my_host() {
    return GasnetNodeMap::get()->my_host();
}

//
// mapping between synthesized handles and requests; this assumes that the
// internal handle exists
//
inline mem_req_t& h2mr(gasnet_handle_t handle) {
    GNTHOR_TRACER(5);
    assert(handle != GASNET_INVALID_HANDLE);
    if (0 < (int64_t) handle) {
    // off-node request
        return g_myqueue.get_request((PeerQueue::size_type)(int64_t)handle-1);
    }

// on-node request
    PeerQueue::size_type index = PeerQueue::size_type(-((int64_t)handle+1));
    return g_myqueue_aux.get_request(index);
}

//
// mapping between external (h) and internal (sh) handles; this assumes that
// the mapping is only called on NEW external handles
//
inline gasnet_handle_t internalize_handle(gasnet_handle_t handle, bool on_node) {
    int64_t index = 0;
    if (on_node) {
        GNTHOR_TRACER(5);
        index = g_myqueue_aux.add_request(handle);
    } else {
        GNTHOR_TRACER(5);
        index = g_myqueue.add_request(handle);
    }
    return internal::synthesize_handle(handle, index, on_node);
}

} // namespace internal

} // namespace gnthor

//
// atomic operations
//
inline uint64_t atomic_fadd(uint64_t * operand, uint64_t incr)
{
    return __sync_fetch_and_add (operand, incr);
}

#endif // ! __GNTHOR_INTERNAL_H__
