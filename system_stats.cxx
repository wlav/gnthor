#include "gnthor/config.h"

#define GNTHOR_INTERNAL 1
#include "gnthor/debug.h"
#include "gnthor/internal.h"
#include "gnthor/system_stats.h"

#include <map>
#include <sstream>
#include <string>

#include <assert.h>
#include <errno.h>
#include <limits.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/types.h>

// Range of measured message sizes; this assumes (based on experimental data) that
// small is roughly the same all-over, and that 512B - 16K is more feature rich.
// Size is limited to 64K, b/c of measurement cost and no indication of either use
// in the applications of interest, or different effects.
const size_t MESSAGE_SIZES[] = {
   8, 32, 64, 128,                     // includes 64B protocol switch-over on Cray
   512, 768, 1024, 1536, 2048, 3072,   // feature-rich block (on Cray)
   4096, 6144, 8192, 12288, 16384,     // includes protocal switch-over on Cray in Gasnet
   32768 };                            // representative enough of all large sizes
const size_t N_MSG_SIZES = sizeof(MESSAGE_SIZES)/sizeof(size_t);

// mapping from round(log(blocksize)*3) to nearest measurement
//  [6.0, 10.0, 12.0, 15.0, 19.0, 20.0, 21.0, 22.0, 23.0, 24.0, 25.0, 26.0, 27.0, 28.0, 29.0, 31.0, 33.0]
const size_t INDEX_MAPPING[] = {
     0,  0,  0,  0,  0,  0,  0,  0,
     1,  1,  1,  2,  2,  3,  3,  3,
     3,  4,  4,  4,  5,  6,  7,  8,
     9, 10, 11, 12, 13, 14, 15, 15
};
const size_t N_IDX_MAPPINGS = sizeof(INDEX_MAPPING)/sizeof(size_t);

#define TIME_SPENT(start, end) (end.tv_sec * 1000000. + end.tv_usec - start.tv_sec*1000000. - start.tv_usec)

#ifndef HOST_NAME_MAX
#define HOST_NAME_MAX 64
#endif

#define GNTHOR_REF_SIZE 512

typedef double latency_t;

// 3 arrays: inj, net, ovl each for put, get == 6
#define NUM_MEAS_ARRAYS 6


//
// indexing functions to allow working with flattened arrays
//
static inline size_t meas_array_size() {
    return gnthor::internal::physical_nodes()*N_MSG_SIZES;
}

static inline size_t meas_array_size_b() {
    return sizeof(latency_t)*meas_array_size();
}


//
// estimate of needed segment offset for calculating stats
//
size_t gnthor::system_stats::estimate_segment_offset() {
// biggest buffer needed is for averaging the measurements or the size of the
// largest "training" message
    size_t desired_offset = gasnet_nodes()*(NUM_MEAS_ARRAYS*meas_array_size_b());
    if (desired_offset < MESSAGE_SIZES[N_MSG_SIZES-1])
       desired_offset = MESSAGE_SIZES[N_MSG_SIZES-1];

    return desired_offset;
}


inline size_t M_INDEX(size_t blocksize) {
    if (blocksize == 0) return 1;
    size_t index = (size_t)round(log(blocksize)*3);
    if (index >= N_IDX_MAPPINGS) index = N_IDX_MAPPINGS - 1;
    return INDEX_MAPPING[index];
}


inline size_t L_INDEX(size_t blocksize, size_t node) {
    return node*N_MSG_SIZES+M_INDEX(blocksize);
}


namespace {
    latency_t* g_put_injection = NULL;
    latency_t* g_put_latencies = NULL;
    latency_t* g_put_overlaps  = NULL;

    latency_t* g_get_injection = NULL;
    latency_t* g_get_latencies = NULL;
    latency_t* g_get_overlaps  = NULL;
}


// Format for writing/reading of latency info:
//  # of total ranks [size_t]      (== gasnet_nodes())
//  # of targets [size_t]          (== physical_nodes())
//  # of message sizes [size_t]    (== N_MSG_SIZES)
//  for [puts, gets]
//     for all nodes:
//        target [size_t:char*]
//        for all sizes:
//           Tinj, Tnet, Tovl, [double[3]]
//           blocksz [size_t]
//           nmease [size_t]
//           
//
namespace {

struct StatRecord {
    StatRecord() : Tinj(0.), Tnet(0.), Tovl(0.), blocksz(0), nmeas(0) {}
    double Tinj, Tnet, Tovl;
    size_t blocksz, nmeas;
} __attribute__((packed));
const size_t STATRECORD_PACKED_SIZE = sizeof(double)*3+sizeof(size_t)*2;

typedef std::map<size_t, StatRecord> BlockStats_t;
typedef std::map<std::string, BlockStats_t> Measurements_t;
Measurements_t gSavedPutMeasurements;
Measurements_t gSavedGetMeasurements;

inline void read_stat_records(FILE* input, Measurements_t& meas, size_t nrecs) {
    StatRecord rec;
    assert(sizeof(StatRecord) == STATRECORD_PACKED_SIZE);
    for (size_t j = 0; j < gnthor::internal::physical_nodes(); ++j) {
        size_t strsz; char buf[HOST_NAME_MAX];
        size_t nitems = fread((char*)&strsz, sizeof(size_t), 1, input);
        if (nitems != 1) goto error;
        nitems = fread(buf, strsz, 1, input);
        if (nitems != 1) goto error;
        buf[strsz] = '\0';
        BlockStats_t& records = meas[buf];
        for (size_t i = 0; i < nrecs; ++i) {
            nitems = fread((char*)&rec, sizeof(StatRecord), 1, input);
            if (nitems != 1) goto error;
        // update existing record, if any
            StatRecord& rr = records[rec.blocksz];
            if (rr.blocksz == 0 /* fresh block */) rr.blocksz = rec.blocksz;
            assert(rr.blocksz == rec.blocksz);
            double divisor = rr.nmeas + rec.nmeas;
            rr.Tinj = (rr.Tinj*rr.nmeas + rec.Tinj*rec.nmeas)/divisor;
            rr.Tnet = (rr.Tnet*rr.nmeas + rec.Tnet*rec.nmeas)/divisor;
            rr.Tovl = (rr.Tovl*rr.nmeas + rec.Tovl*rec.nmeas)/divisor;
            rr.nmeas += rec.nmeas;
        }
    }
    return;
error:
   std::cerr << "corrupted input file ... exiting\n";
   exit(2);
}

inline void write_stat_records(FILE* output, size_t NMEAS,
        latency_t* ainj, latency_t* anet, latency_t* aovl) {
    const size_t rpn   = gnthor::internal::ranks_per_node();
    assert(sizeof(StatRecord) == STATRECORD_PACKED_SIZE);
    StatRecord rec; rec.nmeas = NMEAS;
    for (size_t j = 0; j < gnthor::internal::physical_nodes(); ++j) {
        std::string remote_name = gnthor::internal::get_hostname(j*rpn);
        size_t nsz = remote_name.size();
        fwrite((char*)&nsz, sizeof(size_t), 1, output);
        fwrite(remote_name.c_str(), nsz, 1, output);
        for (size_t i = 0; i < N_MSG_SIZES; ++i) {
            rec.blocksz = MESSAGE_SIZES[i];
            size_t idx = j*N_MSG_SIZES+i;
            rec.Tinj = ainj[idx]; rec.Tnet = anet[idx]; rec.Tovl = aovl[idx];
            fwrite((char*)&rec, sizeof(StatRecord), 1, output);
        }
    }
}

inline void update_measurements(size_t WEIGHT, Measurements_t& meas,
        latency_t* inj, latency_t* net, latency_t* ovl) {
// add weighted average to existing measurements
    const size_t rpn   = gnthor::internal::ranks_per_node();
    for (size_t j = 0; j < gnthor::internal::physical_nodes(); ++j) {
        std::string remote_name = gnthor::internal::get_hostname(j*rpn);
        auto rh = meas.find(remote_name);
        if (rh == meas.end())
            continue;

        for (auto& p : rh->second) {
            assert(p.second.blocksz == p.first && p.second.nmeas);
            StatRecord& r = p.second;
            double divisor = WEIGHT + r.nmeas;
            size_t idx = L_INDEX(p.first, j);
            inj[idx] = (inj[idx]*WEIGHT + r.Tinj*r.nmeas)/divisor;
            net[idx] = (net[idx]*WEIGHT + r.Tnet*r.nmeas)/divisor;
            ovl[idx] = (ovl[idx]*WEIGHT + r.Tovl*r.nmeas)/divisor;
        }
    }
}

} // unnamed namespace


//
// allocate/initialize data structurs; these require the total number of nodes
// to be known, hence needs to be called after gasnet is setup
//
static int initialized = 0;
static void gnthor_latencies_initialize() {
    using namespace gnthor;

    if (initialized)
        return;

// arrays for latency data
    const size_t arrsz = meas_array_size();
    g_put_injection = new latency_t[NUM_MEAS_ARRAYS*arrsz];
    g_put_latencies = g_put_injection+1*arrsz;
    g_put_overlaps  = g_put_injection+2*arrsz;

    g_get_injection = g_put_injection+3*arrsz;
    g_get_latencies = g_put_injection+4*arrsz;
    g_get_overlaps  = g_put_injection+5*arrsz;

// read from file if so directed and available (one per physical node)
    const char* cinput = getenv("GNTHOR_SYSTEMSTATS_FILE");
    if (cinput && (gasnet_mynode() % internal::ranks_per_node()) == 0) {
        assert(sizeof(StatRecord) == STATRECORD_PACKED_SIZE);
        std::stringstream sinput;
        size_t nphys = internal::physical_nodes();
        sinput << cinput << '_' << internal::get_hostname(gasnet_mynode())
               << '_' << gasnet_nodes() << '_' << nphys << ".gntor";
        FILE* input = fopen(sinput.str().c_str(), "rb+");
        if (input) {
            int lres = lockf(fileno(input), F_LOCK, 0);
            if (lres) {
                std::cerr << "read lock on input stats failed ("
                          << strerror(errno) <<
                    "); may get corrupt data when running multiple jobs" << std::endl;
            // not exiting here ...
            }

            size_t header[3];
            while (fread((char*)header, sizeof(size_t), 3, input)) {
                if (header[0] != gasnet_nodes() || header[1] != nphys) {
                    std::cerr << "input stats file (\"" << sinput.str()
                              << "\") is for the wrong configuration!\n";
                    exit(2);
                }
                read_stat_records(input, gSavedPutMeasurements, header[2]);
                read_stat_records(input, gSavedGetMeasurements, header[2]);
            }

            lres = lockf(fileno(input), F_ULOCK, 0);
            fclose(input);
        }
    }

// all good ...
    initialized = 1;
}

namespace {

class Shutdown {
public:
    ~Shutdown() {
         delete[] g_put_injection;
         g_put_injection = g_put_latencies = g_put_overlaps = NULL;
         g_get_injection = g_get_latencies = g_get_overlaps = NULL;
    }
} _shutdown;

} // unnamed namespace


double gnthor::put_injection(size_t target, size_t blocksize) {
    return g_put_injection[L_INDEX(blocksize, target/internal::ranks_per_node())];
}

double gnthor::put_latency(size_t target, size_t blocksize) {
    return g_put_latencies[L_INDEX(blocksize, target/internal::ranks_per_node())];
}

double gnthor::put_estimate(size_t target1, size_t blocksize1, size_t target2, size_t blocksize2) {
    size_t idx1 = L_INDEX(blocksize1, target1/internal::ranks_per_node());
// TODO: interpolate
    double Tinj1 = g_put_latencies[idx1];
    double Tnet1 = g_put_latencies[idx1];
    double pot1  = g_put_injection[idx1];

    size_t idx2 = L_INDEX(blocksize2, target2/internal::ranks_per_node());
// TODO: interpolate
    double Tinj2 = g_put_latencies[idx2];
    double Tnet2 = g_put_latencies[idx2];
    double pot2  = g_put_injection[idx2];

    double v1 = Tinj1 + fmax(Tnet1, Tinj2+Tnet2+(Tnet1-pot1));
    double v2 = Tinj2 + fmax(Tnet2, Tinj1+Tnet1+(Tnet2-pot2));

    if (v1 <= v2 && v2/v1 < 1.03) return 0.;
    if (v2 <  v1 && v1/v2 < 1.03) return 0.;
    return v2 - v1;
}

double gnthor::get_injection(size_t source, size_t blocksize) {
    return g_get_injection[L_INDEX(blocksize, source/internal::ranks_per_node())];
}

double gnthor::get_latency(size_t source, size_t blocksize) {
    return g_get_latencies[L_INDEX(blocksize, source/internal::ranks_per_node())];
}

double gnthor::get_estimate(size_t source1, size_t blocksize1, size_t source2, size_t blocksize2) {
    size_t idx1 = L_INDEX(blocksize1, source1/internal::ranks_per_node());
// TODO: interpolate
    double Tinj1 = g_get_latencies[idx1];
    double Tnet1 = g_get_latencies[idx1];
    double pot1  = g_get_injection[idx1];

    size_t idx2 = L_INDEX(blocksize2, source2/internal::ranks_per_node());
// TODO: interpolate
    double Tinj2 = g_get_latencies[idx2];
    double Tnet2 = g_get_latencies[idx2];
    double pot2  = g_get_injection[idx2];

    double v1 = Tinj1 + fmax(Tnet1, Tinj2+Tnet2+(Tnet1-pot1));
    double v2 = Tinj2 + fmax(Tnet2, Tinj1+Tnet1+(Tnet2-pot2));

    if (v1 <= v2 && v2/v1 < 1.03) return 0.;
    if (v2 <  v1 && v1/v2 < 1.03) return 0.;
    return v2 - v1;
}


static int latencies_done = 0;
int gnthor::collect_latencies()
{
    if (!getenv("GNTHOR_REORDERING_SCHEME") ||    // no reordering in the first place
	!strcmp(getenv("GNTHOR_REORDERING_SCHEME"), "global")) { // needs no stats
       latencies_done = true;
       return 0;
    }

    if (latencies_done)
        return 0;

    struct timeval init_start, init_end;
    gettimeofday(&init_start, NULL);

    gnthor_latencies_initialize();

// TODO: check if _all_ nodes already hit MAXOUT_MEASUREMENTS (has to be all
// b/c skipping a node may make a difference in overall communication if the
// timing runs with fewer nodes simulataneously

// TODO: not sure how to handle the following; just high-jacking the segments
// here assuming that this is early enough it doesn't interfere with other
// application uses (yet)
    int nnodes = gasnet_nodes();
    gasnet_seginfo_t* segment_infos = new gasnet_seginfo_t[nnodes];
    gasnet_getSegmentInfo(segment_infos, nnodes);

// repetitions to average measurements over
    const size_t REPS = 100;

// total number of ranks we share this node with
    int rpn = internal::ranks_per_node();
    size_t allranks = (size_t)gasnet_nodes();

    assert(g_put_injection && "initialize() has not been called (or failed)");

// take measurements for all sizes and nodes (two way)
    char* l_b_ref = (char*)malloc(GNTHOR_REF_SIZE);
    for (size_t idx = 0; idx < N_MSG_SIZES; ++idx) {
        const size_t blocksize = MESSAGE_SIZES[idx];
        char* l_block = (char*)malloc(blocksize);

    // only collect latency from node to node
    // TODO: Realism check: right now all measurements happen simultaneously
    // (likely desired), but only one communicator per node. More probably,
    // the number of communicators should equal the number of servers.
        for (size_t icollect = 0; icollect < internal::physical_nodes(); ++icollect) {
            size_t remote = gasnet_mynode()+rpn/2+icollect*rpn;
            if (remote >= allranks) remote -= allranks;

            struct timeval t_start, t_inter, t_end;

            // if (remote/rpn == mynode)
            //     continue;

        // target shared memory address
            void* r_block = segment_infos[remote].addr;

        // put latencies
            {
            latency_t inj = 0., net = 0.;
            for (size_t i = 0; i < REPS+1; ++i) {
                GASNET_BARRIER;
                gettimeofday(&t_start, NULL);
                gasnet_handle_t handle = gasnet_put_nb(remote, r_block, l_block, blocksize);
                gettimeofday(&t_inter, NULL);
                gasnet_wait_syncnb(handle);
                gettimeofday(&t_end, NULL);
                if (i) inj += TIME_SPENT(t_start, t_inter);
                if (i) net += TIME_SPENT(t_inter, t_end);
            }

        // store results
            g_put_injection[L_INDEX(blocksize, remote/rpn)] = inj/REPS;
            g_put_latencies[L_INDEX(blocksize, remote/rpn)] = net/REPS;

        // put cross latencies with reference
            double total = 0., total_rev = 0.;
            for (size_t i = 0; i < REPS+1; ++i) {
            // one way ...
                GASNET_BARRIER;
                gettimeofday(&t_start, NULL);
                gasnet_handle_t handles[2];
                handles[0] = gasnet_put_nb(remote, r_block, l_block, blocksize);
                handles[1] = gasnet_put_nb(remote, (char*)r_block+blocksize, l_b_ref, GNTHOR_REF_SIZE);
                gasnet_wait_syncnb_all(handles, 2);
                gettimeofday(&t_end, NULL);
                if (i) total += TIME_SPENT(t_start, t_end);

            // and reverse ...
                GASNET_BARRIER;
                gettimeofday(&t_start, NULL);
                handles[0] = gasnet_put_nb(remote, (char*)r_block+blocksize, l_b_ref, GNTHOR_REF_SIZE);
                handles[1] = gasnet_put_nb(remote, r_block, l_block, blocksize);
                gasnet_wait_syncnb_all(handles, 2);
                gettimeofday(&t_end, NULL);
                if (i) total_rev += TIME_SPENT(t_start, t_end);
            }

        // store results
            g_put_overlaps[L_INDEX(blocksize, remote/rpn)] = (total-total_rev)/REPS;
            }

            GASNET_BARRIER;

         // get latencies
            {
            latency_t inj = 0., net = 0.;
            for (size_t i = 0; i < REPS+1; ++i) {
                GASNET_BARRIER;
                gettimeofday(&t_start, NULL);
                gasnet_handle_t handle = gasnet_get_nb(l_block, remote, r_block, blocksize);
                gettimeofday(&t_inter, NULL);
                gasnet_wait_syncnb(handle);
                gettimeofday(&t_end, NULL);
                if (i) inj += TIME_SPENT(t_start, t_inter);
                if (i) net += TIME_SPENT(t_inter, t_end);
            }

         // store results
            g_get_injection[L_INDEX(blocksize, remote/rpn)] = inj/REPS;
            g_get_latencies[L_INDEX(blocksize, remote/rpn)] = net/REPS;

         // get cross latencies with reference
            double total = 0., total_rev = 0.;
            for (size_t i = 0; i < REPS+1; ++i) {
            // one way ...
                GASNET_BARRIER;
                gettimeofday(&t_start, NULL);
                gasnet_handle_t handles[2];
                handles[0] = gasnet_get_nb(l_block, remote, r_block, blocksize);
                handles[1] = gasnet_get_nb(l_b_ref, remote, (char*)r_block+blocksize, GNTHOR_REF_SIZE);
                gasnet_wait_syncnb_all(handles, 2);
                gettimeofday(&t_end, NULL);
                if (i) total += TIME_SPENT(t_start, t_end);

            // and reverse ...
                GASNET_BARRIER;
                gettimeofday(&t_start, NULL);
                handles[0] = gasnet_get_nb(l_b_ref, remote, (char*)r_block+blocksize, GNTHOR_REF_SIZE);
                handles[1] = gasnet_get_nb(l_block, remote, r_block, blocksize);
                gasnet_wait_syncnb_all(handles, 2);
                gettimeofday(&t_end, NULL);
                if (i) total_rev += TIME_SPENT(t_start, t_end);
            }

         // store results
            g_get_overlaps[L_INDEX(blocksize, remote/rpn)] = (total-total_rev)/REPS;
            }

        }

        free(l_block);
    }

    GASNET_BARRIER;
    free(l_b_ref);

// average the results on the lowest rank per physical node
    const size_t bufsize = NUM_MEAS_ARRAYS*meas_array_size_b();
    gasnet_node_t mynode = gasnet_mynode();
    gasnet_put(mynode, segment_infos[mynode].addr, g_put_injection, bufsize);

    GASNET_BARRIER;

    if ((mynode % internal::ranks_per_node()) == 0) {
    // collect all results onto own internal arrays
        const size_t nmeas = NUM_MEAS_ARRAYS*meas_array_size();
        assert(bufsize == sizeof(latency_t)*nmeas);
        latency_t* get_buf = new latency_t[nmeas];
        
        for (size_t i = 0; i < internal::ranks_per_node(); ++i) {
            gasnet_node_t source = gasnet_mynode() + i;
            gasnet_get(get_buf, source, segment_infos[source].addr, bufsize);
            for (size_t j = 0; j < nmeas; ++j)
                g_put_injection[j] += get_buf[j];
        }

        for (size_t j = 0; j < nmeas; ++j)
            g_put_injection[j] /= rpn;

    // write back results if requested
        const char* coutput = getenv("GNTHOR_SYSTEMSTATS_FILE");
        if (coutput) {
            std::stringstream soutput;
            size_t nnodes = gasnet_nodes();
            soutput << coutput << '_' << gnthor::internal::get_hostname(gasnet_mynode())
                    << '_' << nnodes << '_' << internal::physical_nodes() << ".gntor";
            FILE* output = fopen(soutput.str().c_str(), "ab");
            if (output) {
                int lres = lockf(fileno(output), F_LOCK, 0);
                if (lres) {
                    std::cerr << "write lock on input stats failed ("
                              << strerror(errno) <<
                        "); may get corrupt data when running multiple jobs" << std::endl;
                // not exiting here ...
                }

             // write results
                size_t header[3] = {nnodes, internal::physical_nodes(), N_MSG_SIZES};
                fwrite((char*)header, sizeof(size_t), 3, output);
                write_stat_records(output, REPS,
                    g_put_injection, g_put_latencies, g_put_overlaps);
                write_stat_records(output, REPS,
                    g_get_injection, g_get_latencies, g_get_overlaps);

                fflush(output);
                lres = lockf(fileno(output), F_ULOCK, 0);
                fclose(output);
            }
        }

    // average with read-back results
        update_measurements(REPS, gSavedPutMeasurements,
            g_put_injection, g_put_latencies, g_put_overlaps);

        update_measurements(REPS, gSavedGetMeasurements,
            g_get_injection, g_get_latencies, g_get_overlaps);

    // store back for other nodes to pick up
        gasnet_put(mynode, segment_infos[mynode].addr, g_put_injection, bufsize);

        GASNET_BARRIER;
    } else {
        GASNET_BARRIER;

    // retrieve node-level averages
        gasnet_node_t source = mynode/internal::ranks_per_node();
        gasnet_get(g_put_injection, source, segment_infos[source].addr, bufsize);
    }

    GASNET_BARRIER;

    delete[] segment_infos;

    gettimeofday(&init_end, NULL);
    if (gasnet_mynode() == 0)
        std::cerr <<  "GNTHOR - reorder initialization overhead: " << TIME_SPENT(init_start, init_end)/1E6 << "s" << std::endl;

    latencies_done = 1;
    return 0;
}
