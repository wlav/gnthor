#ifndef __GNTHOR_GNTHOR_H__
#define __GNTHOR_GNTHOR_H__

// public API with a few helper functions, usable from client code

#ifdef __cplusplus
extern "C" {
#endif // ifdef __cplusplus

    int gnthor_initialize();
    int gnthor_reset();

    void gnthor_start_comm_region();
    void gnthor_end_comm_region();
    void gnthor_set_immediate_analysis();
    void gnthor_set_deferred_analysis();
    void gnthor_set_keyed_analysis();
    void gnthor_set_no_keyed_analysis();

    void gnthor_print_stats(int);

#ifdef __cplusplus
}
#endif // ifdef __cplusplus

#endif // !__GNTHOR_GNTHOR_H__
