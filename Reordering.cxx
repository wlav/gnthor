#include "gnthor/config.h"

#define GNTHOR_INTERNAL 1
#include "gnthor/sgasnet.h"
#include "gnthor/debug.h"
#include "gnthor/internal.h"

#include "gnthor/Reordering.h"
#include "gnthor/system_stats.h"
#include "gnthor/callstats.h"

#include "gasnet_coll.h"

#include <algorithm>
#include <chrono>
#include <iomanip>
#include <numeric>
#include <utility>
#include <vector>
#include <assert.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

#define BUNDLE_SIZE 8
#define MAX_STATIC_REORDER_BUNDLE_SIZE 32

// offset for meta-info in the reodering packet
const size_t HEADEROFF = 3;

// set the rank that performs the analysis (TODO: should be the fastest
// rank based on global consensus)
const gasnet_node_t ANALYSIS_RANK = 2;


//
// currently available reorderings:
//   - "immediate"  : just issue (no reorder)
//   - "highlow"    : sort by latency, then issue ordered high to low
//   - "lowhigh"    : sort by latency, then issue ordered low to high
//   - "rank_interleave" : mynode%2 high-low, !mynode%2 low-high
//   - "interleave" : mynode%2 interleave high-low (e.g. 1,5,2,6,3,7,4,8)
//                    !mynode%2 interleave low-high
//   - "global"     : use statistical measures to agree on a global schedule
//
std::unique_ptr<gnthor::Reordering> gnthor::Reordering::get_reordering()
{
    std::unique_ptr<Reordering> reordering = nullptr;

    const char* cscheme = getenv("GNTHOR_REORDERING_SCHEME");
    const char* cuse_size = getenv("GNTHOR_REORDERING_BY_SIZE");
    bool use_size = cuse_size && atoi(cuse_size) == 1;
    if (!cscheme || !strcmp(cscheme, "immediate")) {
        cscheme = "immediate";
        reordering = std::unique_ptr<Reordering>(new Immediate{});
    } else if (!strcmp(cscheme, "highlow")) {
        reordering = std::unique_ptr<Reordering>(new Monotonic{BUNDLE_SIZE, use_size, true});
    } else if (!strcmp(cscheme, "lowhigh")) {
        reordering = std::unique_ptr<Reordering>(new Monotonic{BUNDLE_SIZE, use_size, false});
    } else if (!strcmp(cscheme, "rank_interleave")) {
        if (gasnet_mynode()%2)
            reordering = std::unique_ptr<Reordering>(new Monotonic{BUNDLE_SIZE, use_size, true});
        else
            reordering = std::unique_ptr<Reordering>(new Monotonic{BUNDLE_SIZE, use_size, false});
    } else if (!strcmp(cscheme, "interleave")) {
        if (gasnet_mynode()%2)
            reordering = std::unique_ptr<Reordering>(new Interleave{BUNDLE_SIZE, use_size, true});
        else
            reordering = std::unique_ptr<Reordering>(new Interleave{BUNDLE_SIZE, use_size, true});
    } else if (!strcmp(cscheme, "global")) {
        const char* coption = getenv("GNTHOR_GLOBAL_DECISION_INTERVAL");
        int interval = coption ? atoi(coption) : 100;
        coption = getenv("GNTHOR_GLOBAL_DECISION_WARMUP");
        int warmup_skip = coption ? atoi(coption) : 100;
        coption = getenv("GNTHOR_GLOBAL_DECISION_REMEASURE");
        int remeasure = coption ? atoi(coption) : 1000;
        if (!gasnet_mynode()) {
           std::cerr << "[gnthor] Analysis interval:  " << interval    << std::endl;
           std::cerr << "[gnthor] Analysis warmup:    " << warmup_skip << std::endl;
           std::cerr << "[gnthor] Analysis remeasure: " << remeasure   << std::endl;
        }
        reordering = std::unique_ptr<Reordering>(
            new GlobalSchedule{interval, warmup_skip, remeasure});
    } else {
        std::cerr << "selected unknown reordering scheme: " << cscheme << std::endl;
        exit(1);
    }

    if (gasnet_mynode() == 0) {
        std::cerr << "[gnthor] Running \"" << cscheme
		  << std::string(strcmp(cscheme, "global") ?
				 (use_size ? " sizes" : " latencies") : "")
                  << "\" reordering scheme" << std::endl;
    }
    return reordering;
}


// base class
gnthor::Reordering::~Reordering() {}

//
// factor out issueing a bundle of messages
//
size_t gnthor::Reordering::issue_bundle(
        PeerQueue& queue, const std::vector<PeerQueue::size_type>& bundle)
{
    GNTHOR_TRACER(2);

    assert(!bundle.empty());            // make sure check is done externally
    gnthor::gnstats.add_bundle_issued(bundle.size());
    for (auto ib : bundle) {
        mem_req_t& mr = queue.get_request(ib);
        assert(mr.state == r_pending);
        if (mr.type == q_memput) {
            GNTHOR_TRACER(3);
            mr.handle = gasnet_put_nb(mr.target, mr.destination, mr.source, mr.size);
            mr.state  = r_issued;
        } else if (mr.type == q_memput_bulk) {
            GNTHOR_TRACER(3);
            mr.handle = gasnet_put_nb_bulk(mr.target, mr.destination, mr.source, mr.size);
            mr.state  = r_issued;
        } else if (mr.type == q_memput_i) {
            GNTHOR_TRACER(3);
            gasnet_put_nbi(mr.target, mr.destination, mr.source, mr.size);
            mr.handle = GASNET_INVALID_HANDLE;    // makes try_sync(handle) a no-op
            mr.state  = r_issued;
        } else if (mr.type == q_memput_i_bulk) {
            GNTHOR_TRACER(3);
            gasnet_put_nbi_bulk(mr.target, mr.destination, mr.source, mr.size);
            mr.handle = GASNET_INVALID_HANDLE;    // makes try_sync(handle) a no-op
            mr.state  = r_issued;
        } else if (mr.type == q_memget) { 
            GNTHOR_TRACER(3);
            mr.handle = gasnet_get_nb(mr.destination, mr.target, mr.source, mr.size);
            mr.state  = r_issued;
        } else if (mr.type == q_memget_bulk) { 
            GNTHOR_TRACER(3);
            mr.handle = gasnet_get_nb_bulk(mr.destination, mr.target, mr.source, mr.size);
            mr.state  = r_issued;
        } else if (mr.type == q_memget_i) {
            GNTHOR_TRACER(3);
            gasnet_get_nbi(mr.destination, mr.target, mr.source, mr.size);
            mr.handle = GASNET_INVALID_HANDLE;    // makes try_sync(handle) a no-op
            mr.state  = r_issued;
        } else if (mr.type == q_memget_i_bulk) {
            GNTHOR_TRACER(3);
            gasnet_get_nbi_bulk(mr.destination, mr.target, mr.source, mr.size);
            mr.handle = GASNET_INVALID_HANDLE;    // makes try_sync(handle) a no-op
            mr.state  = r_issued;
        }
    }
    return bundle.size();
}

//
// base class completion request simply syncs all outstanding; reorderings can optimize
// e.g. sync only last issued bundle
//
size_t gnthor::Reordering::request_completion(PeerQueue& queue)
{
    GNTHOR_TRACER(2);

    queue.get_bundle_for_completion(-1 /* all */, m_bundle);
    std::vector<PeerQueue::size_type> leftovers; leftovers.reserve(m_bundle.size());
    for (auto ib : m_bundle) {
        mem_req_t& mr = queue.get_request(ib);
        assert(mr.state == r_issued);  // only true for inline server, need wait otherwise
        if (GASNET_OK != gasnet_try_syncnb_nopoll(mr.handle))
            leftovers.push_back(ib);
        else
            mr.state = r_completed;
    }

    for (auto ib : leftovers) {
        mem_req_t& mr = queue.get_request(ib);
        assert(mr.state == r_issued);  // only true for inline server, need wait otherwise
        if (GASNET_OK != gasnet_try_syncnb_nopoll(mr.handle))
            gasnet_wait_syncnb(mr.handle);
        mr.state = r_completed;
    }

// state will be pending when an API call switched off the analysis but not all
// messages had been completed (and now they have)
    if (gnthor::internal::g_state == gnthor::internal::EO_COMPLETION_NEEDED)
        gnthor::internal::g_state = gnthor::internal::EO_ANALYSIS_OFF;

    return m_bundle.size();
}


//
// helper doing latency lookup
//
static inline void get_latencies(double* latencies, gnthor::PeerQueue& queue,
        const std::vector<gnthor::PeerQueue::size_type>& bundle)
{
    using namespace gnthor;
    for (unsigned ib=0; ib<bundle.size(); ++ib) {
        mem_req_t& mr = queue.get_request(bundle[ib]);
        if (mr.type == q_memput)
           latencies[ib] = put_latency(mr.target, mr.size);
        else
           latencies[ib] = get_latency(mr.target, mr.size);
    }
}


//
// helper doing message sizes lookup
//
static inline void get_sizes(size_t* sizes, gnthor::PeerQueue& queue,
        const std::vector<gnthor::PeerQueue::size_type>& bundle)
{
    using namespace gnthor;
    for (unsigned ib=0; ib<bundle.size(); ++ib) {
        mem_req_t& mr = queue.get_request(bundle[ib]);
	sizes[ib] = mr.size;
    }
}

///
/// helper doing high to low sort by latencies
///
static inline void sort_highlow_by_latencies(std::vector<gnthor::PeerQueue::size_type> &bundle,
         double *latencies)
{
    int n = bundle.size();
    while (n) {
        for (int ib = 1; ib < n; ++ib) {
        // sort high to low latency
            if (latencies[ib-1] < latencies[ib]) {
                std::swap(bundle [ib], bundle [ib-1]);
                std::swap(latencies[ib], latencies[ib-1]);
            }
        }
        n -= 1;
    }
}


///
/// helper doing high to low sort by sizes
///
static inline void
sort_highlow_by_sizes(std::vector<gnthor::PeerQueue::size_type> &bundle, size_t *sizes)
{
    int n = bundle.size();
    while (n) {
        for (int ib = 1; ib < n; ++ib) {
        // sort high to low size
            if (sizes[ib-1] < sizes[ib]) {
                std::swap(bundle [ib], bundle [ib-1]);
                std::swap(sizes[ib], sizes[ib-1]);
            }
        }
        n -= 1;
    }
}

///
/// helper doing low to highsort by latencies
///
static inline void sort_lowhigh_by_latencies(std::vector<gnthor::PeerQueue::size_type> &bundle,
         double *latencies)
{
    int n = bundle.size();
    while (n) {
        for (int ib = 1; ib < n; ++ib) {
        // sort low to high latency
            if (latencies[ib-1] > latencies[ib]) {
                std::swap(bundle [ib], bundle [ib-1]);
                std::swap(latencies[ib], latencies[ib-1]);
            }
        }
        n -= 1;
    }
}


///
/// helper doing low to high sort by sizes
///
static inline void
sort_lowhigh_by_sizes(std::vector<gnthor::PeerQueue::size_type> &bundle, size_t *sizes)
{
    int n = bundle.size();
    while (n) {
        for (int ib = 1; ib < n; ++ib) {
        // sort low to high size
            if (sizes[ib-1] > sizes[ib]) {
                std::swap(bundle [ib], bundle [ib-1]);
                std::swap(sizes[ib], sizes[ib-1]);
            }
        }
        n -= 1;
    }
}



//
// initialization for monotonic schedule, request latencies only if not using
//  sizes
//
int gnthor::Monotonic::Initialize()
{
    GNTHOR_TRACER(1);

    if (!m_use_sizes)
	return collect_latencies();
    return 0;
}


//
// monotonic ordered issue (high to low or low to high)
//
size_t gnthor::Monotonic::issue_request_bundled(
        PeerQueue& queue, issue_policy_t policy, bool drain_queue)
{
    GNTHOR_TRACER(2);

// if current bundle does not cover a large enough window, simply return
    if (!queue.ready_for_issue(drain_queue ? -1 : m_bundle_size))
        return 0;

    if (!drain_queue) m_bundle.resize(m_bundle_size);
    queue.get_bundle_for_issue(drain_queue ? -1 : m_bundle_size, m_bundle);

    if (m_bundle.size() == 1)
        return issue_bundle(queue, m_bundle);  // no sort needed

    // sort by sizes or latencies
    if (m_use_sizes) {
	size_t sizes[MAX_STATIC_REORDER_BUNDLE_SIZE];
	get_sizes(sizes, queue, m_bundle);

	// now sort (simple quadratic, as max bundle size is tiny)
	if (m_high_to_low)
	    sort_highlow_by_sizes(m_bundle, sizes);
	else
	    sort_lowhigh_by_sizes(m_bundle, sizes);
    } else {
	// look up all latencies once
	double latencies[MAX_STATIC_REORDER_BUNDLE_SIZE];
	get_latencies(latencies, queue, m_bundle);

	// now sort (simple quadratic, as max bundle size is tiny)
	if (m_high_to_low)
	    sort_highlow_by_latencies(m_bundle, latencies);
	else
	    sort_lowhigh_by_latencies(m_bundle, latencies);
    }

    // sort done, now issue
    return issue_bundle(queue, m_bundle);
}


//
// initialization for interleave schedule, request latencies only if not using
//  sizes
//
int gnthor::Interleave::Initialize()
{
    GNTHOR_TRACER(1);

    if (!m_use_sizes)
	return collect_latencies();
    return 0;
}

//
// interleaved ordered issue
//
size_t gnthor::Interleave::issue_request_bundled(
        PeerQueue& queue, issue_policy_t policy, bool drain_queue)
{
    GNTHOR_TRACER(2);

// if current bundle does not cover a large enough window, simply return
    if (!queue.ready_for_issue(drain_queue ? -1 : m_bundle_size)) {
        return 0;
    }

    if (!drain_queue) m_bundle.resize(m_bundle_size);
    queue.get_bundle_for_issue(drain_queue ? -1 : m_bundle_size, m_bundle);

    if (m_bundle.size() <= 1) {
        return issue_bundle(queue, m_bundle);  // no sort needed
    }

    // sort by sizes or latencies
    if (m_use_sizes) {
	// look up all sizes once
	size_t sizes[MAX_STATIC_REORDER_BUNDLE_SIZE];
	get_sizes(sizes, queue, m_bundle);

#ifdef DEBUG
	std::cerr << "Sizes: ";
	for (unsigned i=0; i<m_bundle.size(); ++i)
	    std::cerr << sizes[i] << " ";
	std::cerr << "\n";
	std::cerr << "Before sort: ";
	for (unsigned i=0; i<m_bundle.size(); ++i)
	    std::cerr << m_bundle[i] << " ";
	std::cerr << "\n";
#endif /* DEBUG */

	// now sort (simple quadratic, as max bundle size is tiny)
	if (m_high_to_low)
	    sort_highlow_by_sizes(m_bundle, sizes);
	else
	    sort_lowhigh_by_sizes(m_bundle, sizes);
    } else {
	// look up all latencies once
	double latencies[MAX_STATIC_REORDER_BUNDLE_SIZE];
	get_latencies(latencies, queue, m_bundle);

#ifdef DEBUG
	std::cerr << "Latencies: ";
	for (unsigned i=0; i<m_bundle.size(); ++i)
	    std::cerr << latencies[i] << " ";
	std::cerr << "\n";
	std::cerr << "Before sort: ";
	for (unsigned i=0; i<m_bundle.size(); ++i)
	    std::cerr << m_bundle[i] << " ";
	std::cerr << "\n";
#endif /* DEBUG */

	// now sort (simple quadratic, as max bundle size is tiny)
	if (m_high_to_low)
	    sort_highlow_by_latencies(m_bundle, latencies);
	else
	    sort_lowhigh_by_latencies(m_bundle, latencies);
    }

// sort done, now interleave
    int bundle_size = m_bundle.size();
    int save_bundle[MAX_STATIC_REORDER_BUNDLE_SIZE];
    for (int ib = 0; ib < bundle_size; ++ib)
	save_bundle[ib] = m_bundle[ib];
    for (int ib = 0; ib < bundle_size - 1; ib += 2) {
	m_bundle[ib] = save_bundle[ib/2];
	m_bundle[ib+1] = save_bundle[bundle_size/2+ib/2];
    }

// interleave done, now issue
    return issue_bundle(queue, m_bundle);
}



//
// initialization for global schedule
//
int gnthor::GlobalSchedule::Initialize()
{
    GNTHOR_TRACER(1);
    gnthor::internal::g_issue_policy = gnthor::internal::EI_DRAIN_ONLY;
    return 0;
}

//
// reset ordering to the next application provided one and redo analysis
//
int gnthor::GlobalSchedule::Reset()
{
    GNTHOR_TRACER(1);

    m_analysis_data.clear();

    return 0;
}


//
// global consensus schedule
//
size_t gnthor::GlobalSchedule::issue_request_bundled(
        PeerQueue& queue, issue_policy_t /* policy */, bool drain_queue)
{
    GNTHOR_TRACER(2);

    if (!queue.ready_for_issue())
        return 0;

// short-circuit if not active
    if (internal::g_state != internal::EO_ANALYSIS_ON) {
        queue.get_bundle_for_issue(-1 /* all */, m_bundle);
        return issue_bundle(queue, m_bundle);
    }

// TODO: this does not play well when there are more messages send before
// draining then the message queue can handle
    if (!drain_queue)
        return 0 ;   // wait until a sync forces the draining

    queue.get_bundle_for_issue(-1 /* all */, m_bundle);

// the analysis done is per (sum(size), sum(neighbors)) for the statistics to make
// sense; which does mean that the behavior across all nodes needs to be similar
// (i.e.switch-over at the same time) or dead-lock will occur (note: this behavior
// is tailored to the GMG and FT programs)
    size_t sum_size = 0, sum_ranks = 0;
    for (auto ib : m_bundle) {
        mem_req_t& mr = queue.get_request(ib);
        sum_size  += mr.size;
        sum_ranks += mr.target;
    }

    m_cur_key = GNTHOR_KEYED_ANALYSIS ? AnalysisKey_t{sum_size, sum_ranks} : AnalysisKey_t{0, 0};
    auto pit = m_analysis_data.find(m_cur_key);
    if (pit == m_analysis_data.end()) {
        auto res = m_analysis_data.emplace(m_cur_key,
            AnalysisData{m_warmup, std::max(m_interval, m_remeasure), m_bundle, queue});
        assert(res.second);
        pit = res.first;
    }
    AnalysisData& ana = pit->second;

#ifndef NDEBUG
    // make sure we always see the same size bundle, otherwise we are not
    // dealing with a fixed domain decomposition
    assert(ana.m_order_mapping.size() == m_bundle.size());

    // likewise, verify that the neigbors haven't changed
    for (auto ib : m_bundle) {
        mem_req_t& mr = queue.get_request(ib);
        bool found = false;
        for (auto p : ana.m_order_mapping) {
            if (p.second == mr.target) {
                found = true;
                break;
            }
        }
        assert(found == true);
    }
#endif

// reorder bundle according to neighbor schedule (note: since reordering starts
// from the very first schedule, this enforces that particular starting order, even
// if the application does not)
    assert(m_bundle.size() == ana.m_order_mapping.size());
    std::vector<PeerQueue::size_type> reordered; reordered.resize(m_bundle.size());
    for (size_t i = 0; i < m_bundle.size(); ++i) {
        assert(ana.m_order_mapping[i].first < reordered.size());
        reordered[ana.m_order_mapping[i].first] = m_bundle[i];
    }
    m_bundle.swap(reordered);

// collect timing statistics
    ana.m_clock_start = AnalysisData::clock_t::now();
    return issue_bundle(queue, m_bundle);
}


size_t gnthor::GlobalSchedule::request_completion(PeerQueue& queue)
{
    GNTHOR_TRACER(2);

    internal::EState state_on_entry = internal::g_state;

    size_t result = Reordering::request_completion(queue);
// short-circuit if not active or pending on entry
// TODO: !result happens b/c sgasnet_wait_syncnb() drains only for the benefit
// of global reordering, but in general need not drain the full queue
    if (state_on_entry == internal::EO_ANALYSIS_OFF || !result)
        return result;

    auto pit = m_analysis_data.find(m_cur_key);
    assert(pit != m_analysis_data.end());
    AnalysisData& ana = pit->second;

    if (ana.m_skip) {
    // warmup is skipped completely (by definition), remeasuring over the larger
    // skip region theoretically should give better statistics, but if there is a
    // trend, it screws things up, so just fully ignore
        ana.m_skip -= 1;
        assert(result == ana.m_order_mapping.size());
        return result;
    } else {
    // done with skip, get going afresh
        if (ana.m_state == AnalysisData::EA_REMEASURE) {
            ana.m_state = AnalysisData::EA_GLOBAL;
            ana.m_best_average = FLT_MAX;   // "forces" do-over
        }
    }

    auto lapsed_time = AnalysisData::clock_t::now() - ana.m_clock_start;
    ana.m_comm_times.push_back(
        std::chrono::duration_cast<std::chrono::microseconds>(lapsed_time).count());

    internal::g_analysis_ready = true;
    if (!GNTHOR_ANALYSIS_IN_BARRIER)
        Analyze();

    assert(result == ana.m_order_mapping.size());
    return result;
}


bool gnthor::GlobalSchedule::Analyze()
{
    GNTHOR_TRACER(2);

    assert(internal::g_analysis_ready);
    internal::g_analysis_ready = false;
    auto pit = m_analysis_data.find(m_cur_key);
    assert(pit != m_analysis_data.end());
    AnalysisData& ana = pit->second;

// timing already taken

// set synced flag if collectives are run
    bool is_synced = false;

    if (ana.m_comm_times.size() == m_interval && ana.m_state != AnalysisData::EA_STOPPED) {
        GNTHOR_TRACER(5)

    // calculate average and std dev of my workload
        double avg = std::accumulate(
            ana.m_comm_times.begin(), ana.m_comm_times.end(), 0.)/(double)ana.m_comm_times.size();
        double stddev = 0.;
        for (auto val : ana.m_comm_times) {
            double s = val - avg;
            stddev += s*s;
        }
        double divisor = ana.m_comm_times.size() == 1 ? 1 : ana.m_comm_times.size() - 1;
        stddev = sqrt(stddev/divisor);

    // send all analysis parts to node ANALYSIS_RANK (only) for it to decide
        if (MAX_COMM_ROUNDS < ana.m_order_mapping.size()) {
            std::cerr << "Increase MAX_COMM_ROUNDS in gnthor/config.h to: "
                      << ana.m_order_mapping.size() << std::endl;
            exit(4);
        }

    // TODO: abstract this getting of segment infos somewhere
        size_t nnodes = gasnet_nodes();
        gasnet_seginfo_t* segment_infos = new gasnet_seginfo_t[nnodes];
        gasnet_getSegmentInfo(segment_infos, nnodes);

        const size_t NBUF=2;
        constexpr size_t BUFSIZE =
            NBUF*sizeof(float)+sizeof(unsigned short)*(MAX_COMM_ROUNDS+1);
        char* buf = (char*)segment_infos[gasnet_mynode()].addr;
#pragma GCC diagnostic ignored "-Wstrict-aliasing"
        *((float*)buf)   = avg;
        *((float*)buf+1) = stddev;
        unsigned short* sbuf = (unsigned short*)((float*)buf + NBUF);
        sbuf[0] = (unsigned short)ana.m_order_mapping.size();
        for (size_t i = 0; i < ana.m_order_mapping.size(); ++i) {
            assert(ana.m_order_mapping[i].first < ana.m_order_mapping.size());
            sbuf[ana.m_order_mapping[i].first+1] = (unsigned short)ana.m_order_mapping[i].second;
        }
#pragma GCC diagnostic pop

        gasnet_coll_gather(GASNET_TEAM_ALL, ANALYSIS_RANK, (char*)segment_infos[ANALYSIS_RANK].addr+BUFSIZE,
            buf, BUFSIZE, GASNET_COLL_SINGLE | GASNET_COLL_SRC_IN_SEGMENT | GASNET_COLL_DST_IN_SEGMENT |
                          GASNET_COLL_IN_MYSYNC | GASNET_COLL_OUT_MYSYNC);

    // broadcast buffer declaration
        constexpr size_t ORDSIZE = MAX_COMM_ROUNDS+HEADEROFF;
        unsigned short* ordbuf = (unsigned short*)segment_infos[gasnet_mynode()].addr;

    // analyze on rank ANALYSIS_RANK (TODO: analyze on measured fastest node)
        if (gasnet_mynode() == ANALYSIS_RANK) {

        // collect all info
            std::vector<float> avg_times; avg_times.reserve(nnodes);
            std::vector<float> read_sd_times; read_sd_times.reserve(nnodes);
            std::vector<std::vector<size_t>> read_orderings{nnodes};

            size_t max_neighbors = 0;
            void* base_addr = (char*)segment_infos[ANALYSIS_RANK].addr+BUFSIZE;
            for (int inode = 0; inode < (int)nnodes; ++inode) {
                void* node_addr = (void*)((char*)base_addr+BUFSIZE*inode);
                avg_times.push_back(*((float*)node_addr));
                read_sd_times.push_back(*((float*)node_addr+1));

                sbuf = (unsigned short*)((float*)node_addr + NBUF);
                size_t nn = (size_t)sbuf[0];
                max_neighbors = std::max(max_neighbors, nn);
                auto& v = read_orderings[inode]; v.reserve(nn);
                for (size_t s = 0; s < nn; ++s)
                    v.push_back(sbuf[s+1]);
            }

        // first time, initialize best order to "no change mapping"
            if (ana.m_order.empty()) {
                ana.m_order.reserve(max_neighbors);
                for (size_t i = 0; i < max_neighbors; ++i)
                    ana.m_order.push_back(i);
           }

        // check for slowest (needed for history)
            size_t slowest_node = (size_t)-1; float slowest_time = -1.f;
            for (size_t i = 0; i < avg_times.size(); ++i) {
                if (slowest_time < avg_times[i]) {
                    slowest_time = avg_times[i];
                    slowest_node = i;
                }
            }

        // move on depending on whether we improved or not
            if (slowest_time < ana.m_best_average) {
                if (GNTHOR_VERBOSE) {
                    fprintf(stderr, "[gnthor:%d] Won! Old: %g; new: %g (%lu is slowest: %ld, %ld)\n",
                            ANALYSIS_RANK, ana.m_best_average, slowest_time, slowest_node,
                            m_cur_key.first, m_cur_key.second);
                }
                ana.m_best_average = slowest_time;

            // typically, only one local round is needed (TODO: tune)
                if (ana.m_state == AnalysisData::EA_LOCAL)
                    ana.m_state = AnalysisData::EA_GLOBAL;

            // save orderings for current use
                ana.m_orderings = std::move(read_orderings);

            // average times are always current (no save)

            // save sd_times for possible use next round, as well as the
            // currently slowest non-edge node
                ana.m_sd_times = std::move(read_sd_times);

                float non_edge_slowest_time = -1.f;
                for (size_t i = 0; i < avg_times.size(); ++i) {
                    if (non_edge_slowest_time < avg_times[i] && \
                            ana.m_orderings[i].size() == max_neighbors) {
                        non_edge_slowest_time = avg_times[i];
                        ana.m_slowest_node = i;
                    }
                }

            } else {     // regression
                if (GNTHOR_VERBOSE) {
                    std::cerr << "[gnthor:" << ANALYSIS_RANK << "] Lost... "
                              << ana.m_best_average << " " << slowest_time << " ("
                              << m_cur_key.first << ", " << m_cur_key.second << ") ... \n";
                }

                if (ana.m_state == AnalysisData::EA_GLOBAL) {
                    ana.m_state = AnalysisData::EA_LOCAL;
                // use stddevs from previous round for this analysis (no change)
                // likewise, restore old order for analysis (no change)
                } else if (ana.m_state == AnalysisData::EA_LOCAL) {
                    ana.m_state = AnalysisData::EA_STOPPED;
                // restore old order (no change)
                } else
                    assert(0);
#ifndef NDEBUG
                read_orderings.clear();
                read_sd_times.clear();
#endif
            }

#ifndef NDEBUG
            if (ana.m_state != AnalysisData::EA_STOPPED) {
                assert(read_orderings.empty() && read_sd_times.empty());
                assert(!ana.m_orderings.empty() && !ana.m_sd_times.empty());
            }
#endif
            bool need_reversal = false;
            if (ana.m_state == AnalysisData::EA_GLOBAL) {       // reorder based on averages
                if (GNTHOR_VERBOSE) {
                    std::cerr << "[gnthor:" << ANALYSIS_RANK << "] Running GLOBAL analysis ("
                              << m_cur_key.first << ", " << m_cur_key.second << ") ... \n";
                }

            // calculate weighted sums; allow only the slowest 50% of nodes to vote; exclude
            // nodes that do not have a full complement of communication partners
                std::vector<std::pair<float, size_t>> votes; votes.resize(max_neighbors);
                for (size_t i = 0; i < votes.size(); ++i) {
                    votes[i].first  = 0.f;
                    votes[i].second = i;
                }

                std::vector<std::pair<float, size_t>> sorted_t; sorted_t.reserve(avg_times.size());
                for (size_t i = 0; i < avg_times.size(); ++i)
                    sorted_t.push_back(std::make_pair(avg_times[i], i));
                std::sort(sorted_t.rbegin(), sorted_t.rend());

                for (size_t i = 0; i < sorted_t.size()/2; ++i) {
                    size_t voting_node = sorted_t[i].second;
                    const auto& order = ana.m_orderings[voting_node];
                    if (order.size() == max_neighbors) {
                        for (size_t iround = 0; iround < max_neighbors; ++iround) {
                            assert(order[iround] < avg_times.size());
                            votes[iround].first += avg_times[order[iround]];
                        }
                    }
                }

            // sort rounds by weighted value
                std::sort(votes.rbegin(), votes.rend());

            // save new order
                ana.m_order.clear(); ana.m_order.reserve(votes.size());
                for (auto& v : votes)
                    ana.m_order.push_back(v.second);

            } else if (ana.m_state == AnalysisData::EA_LOCAL) { // use std dev for slowest neighborhood

            // check for slowest, non-edge only
                if (GNTHOR_VERBOSE) {
                    std::cerr << "[gnthor:" << ANALYSIS_RANK << "] Running LOCAL analysis for node: "
                              << ana.m_slowest_node << " (" << m_cur_key.first << ", "
                              << m_cur_key.second << ") ... \n";
                }

            // retrieve order of slowest node and sort
                const auto& slowest_order = ana.m_orderings[ana.m_slowest_node];

            // vote based on standard deviation
                std::vector<std::pair<float, size_t>> votes;
                votes.resize(slowest_order.size());
                for (size_t iround = 0; iround < slowest_order.size(); ++iround) {
                   assert(slowest_order[iround] < ana.m_sd_times.size());
                   votes[iround].first = ana.m_sd_times[slowest_order[iround]];
                   votes[iround].second = iround;
                }

            // sort and save
                std::sort(votes.rbegin(), votes.rend());

            // we worked on info from a previous round, so require a reversal before
            // applying new order
                need_reversal = true;

            // save new order
                ana.m_order.clear(); ana.m_order.reserve(votes.size());
                for (auto& v : votes)
                    ana.m_order.push_back(v.second);
 
            } else if (ana.m_state == AnalysisData::EA_STOPPED) {
                if (ana.m_best_average <= slowest_time) {
                // got here b/c of overshoot: restore order
                    if (GNTHOR_VERBOSE)
                        std::cerr << "[gnthor:" << ANALYSIS_RANK << "] Analysis STOPPED and REVERTING!\n";

                    need_reversal = true;

                // the reversal restores; for simplicity, apply a no-op reorder
                    assert(ana.m_order.size() == max_neighbors);
                    for (size_t i = 0; i < max_neighbors; ++i)
                        ana.m_order[i] = i;
                } else
                    if (GNTHOR_VERBOSE) std::cerr << "[gnthor:" << ANALYSIS_RANK << "] Analysis STOPPED\n";
            } else {
                assert(0);
            }

            assert(!ana.m_order.empty());

            if (GNTHOR_VERBOSE && !need_reversal) {
                std::cerr << "[gnthor:" << ANALYSIS_RANK << "] Order permutation: ";
                for (auto o : ana.m_order)
                    std::cerr << o << " ";
                std::cerr << std::endl;
            }

        // store back updated ordering for other nodes to get
            ordbuf[0] = (unsigned short)ana.m_state;
            ordbuf[1] = (unsigned short)need_reversal;
            ordbuf[2] = (unsigned short)ana.m_order.size();
            for (size_t i = 0; i < ana.m_order.size(); ++i)
                ordbuf[i+HEADEROFF] = ana.m_order[i];
            assert(ana.m_order.size() == max_neighbors);
        }

    // broadcast decision
        unsigned short* ordresult = (unsigned short*)segment_infos[gasnet_mynode()].addr + ORDSIZE;
        gasnet_coll_broadcast(GASNET_TEAM_ALL, ordresult, ANALYSIS_RANK,
            ordbuf, ORDSIZE*sizeof(unsigned short),
            GASNET_COLL_SINGLE | GASNET_COLL_SRC_IN_SEGMENT | GASNET_COLL_DST_IN_SEGMENT |
            GASNET_COLL_IN_MYSYNC | GASNET_COLL_OUT_MYSYNC);

    // we are now properly synced
        is_synced = true;

        ana.m_state = (AnalysisData::AnalysisState)ordresult[0];

        if (ordresult[1]) {   // analysis had a reversal
            assert(!ana.m_last_order_mapping.empty());
            ana.m_order_mapping = ana.m_last_order_mapping;
        } else {              // no reversal: store history before reordering
            assert(!ana.m_order_mapping.empty());
            ana.m_last_order_mapping = ana.m_order_mapping;
        }

    // apply the newly given order as a permutation
        size_t norder = ordresult[2];
        assert(!norder || ana.m_order_mapping.size() <= norder);
        for (auto& p : ana.m_order_mapping) {
            size_t ioffset = 0;
            for (size_t i = 0; i < norder; ++i) {
                size_t newpos = ordresult[i+HEADEROFF];
                if (ana.m_order_mapping.size() <= newpos) {
                    ioffset += 1;  // position to skip
                    continue;
                }
                if (p.first == newpos) {
                    p.first = i-ioffset;
                    break;
                }
           }

#ifndef NDEBUG
        // check that all messages occur once and only once
            std::vector<size_t> verifier(ana.m_order_mapping.size());
            for (auto& p : ana.m_order_mapping) {
                verifier[p.first] = 1;
            }
            for (auto& i : verifier) {
                if (i != 1) {
                    std::cerr << gasnet_mynode() << " ORDERING FAILED " << ana.m_order_mapping.size() << " " << norder << " ";
                    for (size_t i = 0; i < norder; ++i) {
                        std::cerr << ordresult[i+HEADEROFF] << " ";
                    }
                    std::cerr << std::endl;
                }
                assert(i == 1);
            }
#endif
        }

    // TODO: don't want to lookup seg infos all the time ...
        delete[] segment_infos;
        ana.m_comm_times.clear();

     // now reset skip period to allow remeasurement
        if (ana.m_state == AnalysisData::EA_STOPPED && m_remeasure) {
            ana.m_skip  = m_remeasure;
            ana.m_state = AnalysisData::EA_REMEASURE;
            if (GNTHOR_VERBOSE && gasnet_mynode() == ANALYSIS_RANK)
                std::cerr << "[gnthor:" << ANALYSIS_RANK << "] Will remeasure in " << ana.m_skip << " rounds\n";
        }
    }

    return is_synced;
}
