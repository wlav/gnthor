// File: setup.inc
// Author: Wim Lavrijsen (WLavrijsen@lbl.gov)

// Basic test setup, common portion.
//


// see: https://bitbucket.org/berkeleylab/gasnet README for an example code of
// mixing MPI and gasnet
    gasnet_init(&argc, &argv);

    size_t segment_size = 256*1024*1024; // 256MB
    size_t segment_max = gasnet_getMaxGlobalSegmentSize();
    if (segment_size > segment_max) 
        segment_size = segment_max; // check later whether it's sufficient

    gasnet_attach(NULL, 0, segment_size, 0);

    if (BLOCKSIZE % 8 != 0) {
        std::cerr << "BLOCKSIZE (" << BLOCKSIZE << ") needs to be a multiple of 8 for alignment" << std::endl;
        exit(1);
    }

    Data data{BLOCKSIZE};

    int mynode = gasnet_mynode();
    gasnet_seginfo_t* segment_infos = (gasnet_seginfo_t*)malloc(gasnet_nodes()*sizeof(gasnet_seginfo_t));
    gasnet_getSegmentInfo(segment_infos, gasnet_nodes());
    if (gasnet_nodes()*sizeof(double)*2 > segment_infos[mynode].size) {
        std::cerr << "Segment size (" << segment_infos[mynode].size << ") too small to accomodate messages.\n";
        exit(2);
    }

// bookkeeping
    if (mynode == 0) {
        typedef typename std::chrono::steady_clock::period P;
        typedef typename std::ratio_multiply<P, std::mega>::type TT;
        std::cerr << "CLOCK precision: " << std::fixed << double(TT::num)/TT::den << " us\n";

        std::cerr << "BLOCK SIZE: " << BLOCKSIZE << std::endl;
        std::cerr << "Running " << EXPERIMENTS << " experiment of " << ITERATIONS << " each\n";
#ifdef MEASURE_PUT
        std::cerr << "Measuring PUTs\n";
#else
        std::cerr << "Measuring GETs\n";
#endif
    }

    GASNET_BARRIER

    int isMPIinit = 0;
    if (MPI_Initialized(&isMPIinit) != MPI_SUCCESS) { /* test if MPI already init */
        std::cerr << "Error calling MPI_Initialized()\n";
        exit(3);
    }
    if (!isMPIinit /* MPI not init, so do it */ && MPI_Init(&argc, &argv) != MPI_SUCCESS) {
        std::cerr << "Error calling MPI_Init()\n";
        exit(4);
    }

// set affinity and define current CPU (only needed on Shepard: rely on srun on Cori/Edison)
#ifdef SHEPARD_
    wlav::MachineState::state().setAffinity();
// run at peak (NOT good for mxm, but fine on ibv)
    wlav::MachineState::state().setNodeFreq(wlav::highest);
#endif

    GASNET_BARRIER

// for simplicity, demand square
    int isr = int(sqrt((double)gasnet_nodes()));
    if (isr*isr != gasnet_nodes()) {
       std::cerr << "Require total ranks to be a square ... exiting\n";
       exit(3);
    }

// prepare
    int imynode = (int)mynode;
    int irowsz = (int)sqrt(gasnet_nodes());
    int imyrow = imynode / irowsz;
    Partner west{"west", gasnet_node_t((imynode-1) / irowsz < imyrow ? -1 : imynode-1), segment_infos};
    Partner north{"north", gasnet_node_t((imynode-irowsz) < 0 ? -1 : imynode-irowsz), segment_infos};
    Partner east{"east", gasnet_node_t((imynode+1) / irowsz > imyrow ? -1 : imynode+1), segment_infos};
    Partner south{"south", gasnet_node_t((imynode+irowsz) >= (int)gasnet_nodes() ? -1 : imynode+irowsz), segment_infos};

    if (VERBOSE) {
        std::ostringstream msg;
        msg  << "Neighbors (W, N, E, S) for " << mynode
             << ' ' << (int)west.node
             << ' ' << (int)north.node
             << ' ' << (int)east.node
             << ' ' << (int)south.node << '\n';
        std::cerr << msg.str() << std::flush;
    }

    GASNET_BARRIER

// actual jobs start here
    std::chrono::steady_clock::duration fulljob = std::chrono::steady_clock::duration::zero();
    auto myjob_start = std::chrono::steady_clock::now();
