#include "wlav_utils/machine.h"                                              

#include <assert.h>
#include <limits.h>
#include <math.h>  
#include <stdint.h>
#include <stdio.h> 
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>

#include <algorithm>
#include <chrono>
#include <iostream>
#include <iomanip> 
#include <numeric> 
#include <sstream> 
#include <utility> 
#include <vector>  

#define GASNET_PAR 1
#include <gasnet.h>

#ifdef SHEPARD_
#ifdef __PLATFORM_COMPILER_GNU_VERSION_STR
#undef __PLATFORM_COMPILER_GNU_VERSION_STR
#endif
#endif

#include <mpi.h>

#define BARRIER_BUNDLE_SYNC 1
#define MEASURE_PUT 1

#ifdef MEASURE_PUT
#define GASNET_OPERATION(a1, a2, a3, a4) gasnet_put_nb((a1), (a2), (a3), (a4))
#else      // measure get
#define GASNET_OPERATION(a1, a2, a3, a4) gasnet_get_nb((a3), (a1), (a2), (a4))
#endif

#define TIME_SPENT(start, end) (end.tv_sec * 1000000. + end.tv_usec - start.tv_sec*1000000. - start.tv_usec)

#define GASNET_BARRIER \
   gasnet_barrier_notify(0, GASNET_BARRIERFLAG_ANONYMOUS);\
   gasnet_barrier_wait(0, GASNET_BARRIERFLAG_ANONYMOUS);

#define WLAV_ENVVAL(name, defval) \
   int name = getenv("WLAV_"#name) ? atoi(getenv("WLAV_"#name)) : defval;

#define WLAV_ENVVAL2(name) \
   if (getenv("WLAV_"#name)) name = atoi(getenv("WLAV_"#name));

const bool VERBOSE = false;

static int ITERATIONS      = 100;
static int EXPERIMENTS     = 1;
static int EXPERIMENT_SIZE = 256;
static int MAX_INJECTION   = 3;
static int MSG_OVERLAP     = 1;

struct Partner {
   Partner(int offset, gasnet_seginfo_t* segment_infos) {
      rank  = (gasnet_mynode() + offset + 1) % gasnet_nodes();
      segment = (char*)segment_infos[rank].addr;
   }
   int   rank;
   char* segment;
};

struct Data {
   Data(int s1, int s2) : size1(s1), size2(s2) { posix_memalign(&data, 8, std::max(s1, s2)); }
   ~Data() { free(data); }
   int size1;
   int size2;
   void* data;

private:
   Data(const Data&) = delete;
   Data& operator=(const Data&) = delete;
};

void spin_wheels(const Partner& p, Data& d) {
   gasnet_handle_t handles[EXPERIMENT_SIZE];

   for (int k = 0; k < EXPERIMENT_SIZE; k++)
      handles[k] = GASNET_OPERATION(p.rank, p.segment+k*d.size1, d.data, d.size1);
   gasnet_wait_syncnb_all(handles, EXPERIMENT_SIZE);

   for (int k = 0; k < EXPERIMENT_SIZE; k++)
      handles[k] = GASNET_OPERATION(p.rank, p.segment+k*d.size2, d.data, d.size2);
   gasnet_wait_syncnb_all(handles, EXPERIMENT_SIZE);
}

void bundled_run(const Partner& p1, const Partner& p2, Data& d, int BS,
                 double* injection_time, double* network_time, double* total_time) {
   timeval total1, total2;
   std::chrono::steady_clock::duration injection_duration = std::chrono::steady_clock::duration::zero();
   std::chrono::steady_clock::duration network_duration   = std::chrono::steady_clock::duration::zero();
   std::chrono::steady_clock::duration total_duration     = std::chrono::steady_clock::duration::zero();
   gasnet_handle_t* handles = new gasnet_handle_t[MAX_INJECTION];

   if (EXPERIMENT_SIZE != (EXPERIMENT_SIZE/BS)*BS) {
      fprintf(stderr, "ERROR: experiment size (%d) must be a multiple of bundle size (%d)\n", EXPERIMENT_SIZE, BS);
      return;
   }

   GASNET_BARRIER

   int step = d.size1 > d.size2 ? d.size1 : d.size2;
   int current_injected = 0; bool untimed_injected = false; uint64_t total_send = 0;

   if (VERBOSE && gasnet_mynode() == 0)
      printf("running bundled_run with size: %d\n", BS);

   gettimeofday(&total1, NULL);
   for (int i = 0; i < ITERATIONS; ++i) {
#ifdef BARRIER_BUNDLE_SYNC
      GASNET_BARRIER
#endif

      auto total_start  = std::chrono::steady_clock::now();
      auto inject_start = std::chrono::steady_clock::now();

      for (int k = 0; k < EXPERIMENT_SIZE/BS; k++) {
         int use_size = k%2 ? d.size1 : d.size2;
         const Partner& p = k%2 ? p1 : p2;
         for (int jj = 0; jj < BS; ++jj) {
            handles[current_injected] = GASNET_OPERATION(p.rank, p.segment+(BS*k+jj)*step, d.data, use_size);
            untimed_injected = true;
            total_send += use_size;
            if (++current_injected == MAX_INJECTION) {
            // injection pause
               injection_duration += std::chrono::steady_clock::now() - inject_start;

            // wait for outstanding messages (minus requested overlap)
               auto network_start = std::chrono::steady_clock::now();
               gasnet_wait_syncnb_all(handles, MAX_INJECTION-MSG_OVERLAP);
               network_duration += std::chrono::steady_clock::now() - network_start;

            // track the remaining outstanding handles
               for (int imsg = 0; imsg < MSG_OVERLAP; ++imsg)
                  handles[imsg] = handles[MAX_INJECTION-MSG_OVERLAP+imsg];
               current_injected = MSG_OVERLAP;

            // injecton timer restart
               untimed_injected = false;
               inject_start = std::chrono::steady_clock::now();
            }
         }
      }

   // stop injection time
      if (untimed_injected) {
         injection_duration += std::chrono::steady_clock::now() - inject_start;
         untimed_injected = false;
      }

   // wait on any left-over handles outstanding (MSG_OVERLAP)
      if (current_injected) {
         auto network_start = std::chrono::steady_clock::now();
         gasnet_wait_syncnb_all(handles, current_injected);
         network_duration += std::chrono::steady_clock::now() - network_start;
         current_injected = 0;
      }

   // stop total time
      total_duration += std::chrono::steady_clock::now() - total_start;
   }
   gettimeofday(&total2, NULL);

   GASNET_BARRIER

   uint64_t expected = ((d.size1+d.size2)/2.)*ITERATIONS*EXPERIMENT_SIZE;
   if (total_send != expected)
      fprintf(stderr, "ERROR: expected to send %lu, but actual sent is %lu\n", expected, total_send);

   *injection_time += std::chrono::duration_cast<std::chrono::microseconds>(injection_duration).count();
   *network_time   += std::chrono::duration_cast<std::chrono::microseconds>(network_duration).count();
   //*total_time += TIME_SPENT(total1, total2);
   *total_time     += std::chrono::duration_cast<std::chrono::microseconds>(total_duration).count();
   delete [] handles;
}


int main( int argc, char* argv[] ) {
// see: https://bitbucket.org/berkeleylab/gasnet README for an example code of
// mixing MPI and gasnet

   WLAV_ENVVAL2(ITERATIONS);
   WLAV_ENVVAL2(EXPERIMENTS);
   WLAV_ENVVAL2(EXPERIMENT_SIZE);
   WLAV_ENVVAL2(MAX_INJECTION);
   WLAV_ENVVAL2(MSG_OVERLAP);

   WLAV_ENVVAL(SMALLBLOCK,           8);
   WLAV_ENVVAL(LARGEBLOCK,       32768);
   WLAV_ENVVAL(MINBUNDLESIZE,        1);
   WLAV_ENVVAL(MAXBUNDLESIZE,      128);
   WLAV_ENVVAL(RANKOFF_1,     NUMCORES);
   WLAV_ENVVAL(RANKOFF_2,     NUMCORES);

   gasnet_init(&argc, &argv);

   size_t segment_size = 64*1024*1024; // 64MB
   size_t segment_max = gasnet_getMaxGlobalSegmentSize();
   if (segment_size > segment_max) segment_size = segment_max;

   gasnet_attach(NULL, 0, segment_size, 0);

   if (RANKOFF_1 == -1)
      RANKOFF_1 = gasnet_nodes()/2;
   if (RANKOFF_2 == -1)
      RANKOFF_2 = gasnet_nodes()/2;

   int mynode = gasnet_mynode();
   gasnet_seginfo_t* segment_infos = (gasnet_seginfo_t*)malloc(gasnet_nodes()*sizeof(gasnet_seginfo_t));
   gasnet_getSegmentInfo(segment_infos, gasnet_nodes());
   Partner partner1(RANKOFF_1, segment_infos), partner2(RANKOFF_2, segment_infos);
   double* zero_segment = (double*)segment_infos[0].addr;       // for collecting results
   if (LARGEBLOCK*EXPERIMENT_SIZE > (int)segment_infos[partner1.rank].size ||
       LARGEBLOCK*EXPERIMENT_SIZE > (int)segment_infos[partner2.rank].size ||
       LARGEBLOCK*EXPERIMENT_SIZE > (int)segment_infos[mynode].size) {
      fprintf(stderr, "Segment sizes (%lu, %lu, %lu) too small to accomodate messages.\n",
                      segment_infos[partner1.rank].size, segment_infos[partner2.rank].size,
                      segment_infos[mynode].size);
      exit(1);
   }

   int bundle_first = int(log2(MINBUNDLESIZE));
   int bundle_last  = int(log2(MAXBUNDLESIZE))+1;
   int bundle_range = bundle_last - bundle_first;
   if (mynode == 0 && (gasnet_nodes()*sizeof(double)*bundle_range*3) > segment_infos[0].size) {
      fprintf(stderr, "Segment size (%lu) to small to receive results (%lu).\n",
             segment_infos[0].size, (gasnet_nodes()*sizeof(double)*bundle_range));
      exit(2);
   }
   free(segment_infos);

   GASNET_BARRIER

   int isMPIinit = 0;
   if (MPI_Initialized(&isMPIinit) != MPI_SUCCESS) { /* test if MPI already init */
      fprintf(stderr, "Error calling MPI_Initialized()\n");
      exit(2);
   }
   if (!isMPIinit /* MPI not init, so do it */ && MPI_Init(&argc, &argv) != MPI_SUCCESS) {
      fprintf(stderr, "Error calling MPI_Init()\n");
      exit(3);
   }

// bookkeeping
   if (mynode == 0) {
      typedef typename std::chrono::steady_clock::period P;
      typedef typename std::ratio_multiply<P, std::mega>::type TT;
      std::cout << "CLOCK precision: " << std::fixed << double(TT::num)/TT::den << " us" << std::endl;

      printf("BLOCK SIZES: %d and %d, OFFSETS: %d and %d, BUNDLE RANGE: %d-%d, MAX INJECTION: %d, MSG OVERLAP: %d\n",
             SMALLBLOCK, LARGEBLOCK, RANKOFF_1, RANKOFF_2, MINBUNDLESIZE, MAXBUNDLESIZE, MAX_INJECTION, MSG_OVERLAP);
      printf("Running %d EXPERIMENTS of %d ITERATIONS each\n",
             EXPERIMENTS, ITERATIONS);
#ifdef BARRIER_BUNDLE_SYNC
      printf("Iterations are BARRIERS SYNC-ed\n");
#else
      printf("NO BARRIER between iterations\n");
#endif // BARRIER_BUNDLE_SYNC
#ifdef MEASURE_PUT
      printf("Measuring PUTs\n");
#else
      printf("Measuring GETs\n");
#endif
   }

// set affinity and define current CPU
   wlav::MachineState::state().setAffinity();
// run at peak (NOT good for mxm, but fine on ibv)
   wlav::MachineState::state().setNodeFreq(wlav::highest);

   GASNET_BARRIER

// actual jobs start here
   timeval fulljob1, fulljob2;
   gettimeofday(&fulljob1, NULL);

// buffers to use
   Data data(SMALLBLOCK, LARGEBLOCK);

// spin the wheels a bit to get past any one-off initialization
   spin_wheels(partner1, data);
   if (partner1.rank != partner2.rank)
      spin_wheels(partner2, data);

   GASNET_BARRIER

// collect measurements
   double* injection_times = new double[bundle_range];
   double* network_times   = new double[bundle_range];
   double* total_times     = new double[bundle_range];
   for (int i = 0; i < bundle_range; ++i)
      injection_times[i] = network_times[i] = total_times[i] = 0.;

   for (int i = 0; i < EXPERIMENTS; ++i) {
      for (int j = bundle_first; j < bundle_last; ++j) {
         int idx = j - bundle_first;
         bundled_run(partner1, partner2, data, (int)pow(2, j), &injection_times[idx], &network_times[idx], &total_times[idx]);
      }
   }

   if (VERBOSE && mynode == 0) {
      printf("Results (node 0):\n");
      for (int j = bundle_first; j < bundle_last; ++j) {
         int idx = j - bundle_first;
         printf("\tbundle size: %4d\tTinj: %.0f\tTnet: %.0f\tTtot: %.0f\n",
                (int)pow(2, j), injection_times[idx], network_times[idx], total_times[idx]);
      }
   }

   GASNET_BARRIER

// push all results onto node 0's segment
   if (mynode != 0) {
      gasnet_put(0, &zero_segment[(mynode*3  )*bundle_range], injection_times, bundle_range*sizeof(double));
      gasnet_put(0, &zero_segment[(mynode*3+1)*bundle_range], network_times,   bundle_range*sizeof(double));
      gasnet_put(0, &zero_segment[(mynode*3+2)*bundle_range], total_times,     bundle_range*sizeof(double));
   }

   GASNET_BARRIER

   if (mynode == 0) {
      int allranks = gasnet_nodes(); 
      for (int i = 1; i < allranks; ++i) {
         for (int j = 0; j < bundle_range; ++j) {
            injection_times[j] += *(zero_segment+(i*3  )*bundle_range+j);
            network_times[j]   += *(zero_segment+(i*3+1)*bundle_range+j);
            total_times[j]     += *(zero_segment+(i*3+2)*bundle_range+j);
         }
      }

      for (int j = 0; j < bundle_range; ++j) {
         injection_times[j] /= allranks * EXPERIMENTS;
         network_times[j]   /= allranks * EXPERIMENTS;
         total_times[j]     /= allranks * EXPERIMENTS;
      }

      printf("Average results:\n");
      for (int j = bundle_first; j < bundle_last; ++j) {
         int idx = j - bundle_first;
         printf("\tbundle size: %4d\tTinj: %.0f\tTnet: %.0f\tTtot: %.0f\tTsum: %.0f\n",
                (int)pow(2, j), injection_times[idx], network_times[idx], total_times[idx], injection_times[idx]+network_times[idx]);
      }
   }

   delete [] total_times;
   delete [] network_times;
   delete [] injection_times;

   GASNET_BARRIER

// done
   gettimeofday(&fulljob2, NULL);
   if (mynode == 0)
      printf("job duration: %.2f seconds\n", TIME_SPENT(fulljob1, fulljob2)/1E6);

   MPI_Barrier(MPI_COMM_WORLD);
   if (!isMPIinit) MPI_Finalize();

   gasnet_exit(0);

   return 0;
} 
