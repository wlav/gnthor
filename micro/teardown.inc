// File: teardown.inc
// Author: Wim Lavrijsen (WLavrijsen@lbl.gov)

// Basic test teardown, common portion
//


// see: https://bitbucket.org/berkeleylab/gasnet README for an example code of
// mixing MPI and gasnet
    GASNET_BARRIER

    fulljob += std::chrono::steady_clock::now() - myjob_start;
    if (mynode == 0)
        std::cerr << "job duration: " << std::chrono::duration_cast<std::chrono::seconds>(fulljob).count() << " seconds\n";

    free(segment_infos);

    MPI_Barrier(MPI_COMM_WORLD);
    if (!isMPIinit) MPI_Finalize();

    gasnet_exit(0);
