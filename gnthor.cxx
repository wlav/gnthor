#include "gnthor/config.h"

#define GNTHOR_INTERNAL 1
#include "gnthor/sgasnet.h"
#include "gnthor/debug.h"
#include "gnthor/gnthor.h"
#include "gnthor/internal.h"
#include "gnthor/Reordering.h"

#include "gasnet_coll.h"

bool GNTHOR_VERBOSE = false;
bool GNTHOR_CAPTURE_LOCAL = false;
bool GNTHOR_ANALYSIS_IN_BARRIER = false;
bool GNTHOR_KEYED_ANALYSIS = true;

//
// entry-point, needs to be called just after attaching gasnet (and is currently
// called by sgasnet_attach)
//
int gnthor_initialize()
{
    GNTHOR_TRACER(1);

// force initialization of host name (TODO: this is a fugly place; but it has to
// be a) done by all nodes and b) done after gasnet_attach() ... )
    gnthor::internal::get_hostname(gasnet_mynode());

// collections initialization
#if GASNET_PAR
#error NOT SUPPORTING PAR: REQUIRES IMAGE ARRAY FOR COLLECTIVES
#endif

#ifndef GNTHOR_USES_UPCC
// if using upcc, that runtime will initialize, otherwise we do it
    gasnet_coll_init(NULL, 0, NULL, 0, 0);
#endif

// whether we want debugging output or not
    const char* coption = getenv("GNTHOR_VERBOSE");
    if (coption && atoi(coption)) GNTHOR_VERBOSE = true;

// whether to capture messages send to the local machine
    coption = getenv("GNTHOR_CAPTURE_LOCAL");
    if (coption && atoi(coption)) GNTHOR_CAPTURE_LOCAL = true;

// whether to drive gnthor from the client, or self-drive (== default)
    coption = getenv("GNTHOR_API_DRIVEN");
    if (coption && atoi(coption)) gnthor::internal::g_state = gnthor::internal::EO_ANALYSIS_OFF;

// whether to analyze inside barriers or on regular interval (TODO: auto-detect)
    coption = getenv("GNTHOR_ANALYSIS_IN_BARRIER");
    if (coption) GNTHOR_ANALYSIS_IN_BARRIER = bool(atoi(coption));

// reorder-schedule specific information (also creates reordering itself, allowing
// this to be done prior to client code)
    return gnthor::Reordering::get()->Initialize();
}


//
// allow a (reorder-specific) "system reset"
//
int gnthor_reset()
{
    GNTHOR_TRACER(1);
    if (GNTHOR_VERBOSE && !gasnet_mynode())
        std::cerr << "[gnthor:0] resetting reordering\n";
    return gnthor::Reordering::get()->Reset();
}


//
// start a communication region where we are to reorder
//
void gnthor_start_comm_region()
{
    GNTHOR_TRACER(1);
// if we get here while pending, then that means there is no sync outside of
// the communication region, which is a performance bug
    assert(gnthor::internal::g_state != gnthor::internal::EO_COMPLETION_NEEDED);
    gnthor::internal::g_state = gnthor::internal::EO_ANALYSIS_ON;
}

//
// finish a communication region where we are to reorder
//
void gnthor_end_comm_region()
{
    GNTHOR_TRACER(1);
// finish up issueing all outstanding requests, but the communication completion
// will come later when the client application calls it
    if (gnthor::internal::issue_pending_requests()) {
        gnthor::internal::g_state = gnthor::internal::EO_COMPLETION_NEEDED;
    } else
    // nothing further to sync, so done
        gnthor::internal::g_state = gnthor::internal::EO_ANALYSIS_OFF;
}


//
// run the analysis, after enough data is collected, immediately after waitsync
//
void gnthor_set_immediate_analysis()
{
    GNTHOR_TRACER(1);
    GNTHOR_ANALYSIS_IN_BARRIER = false;
}

//
// defer the analysis, after enough data is collected, into the next barrier
//
void gnthor_set_deferred_analysis()
{
    GNTHOR_TRACER(1);
    GNTHOR_ANALYSIS_IN_BARRIER = true;
}

//
// switch on/off analysis on (size, neighbor) pairs
//
void gnthor_set_keyed_analysis()
{
    GNTHOR_TRACER(1);
    GNTHOR_KEYED_ANALYSIS = true;
}

void gnthor_set_no_keyed_analysis()
{
    GNTHOR_TRACER(1);
    GNTHOR_KEYED_ANALYSIS = false;
}


// gnthor_print_stats(int) lives in callstats.cxx
