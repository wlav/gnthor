#include "gnthor/config.h"

#define GNTHOR_INTERNAL 1
#include "gnthor/sgasnet.h"
#include "gnthor/debug.h"
#include "gnthor/gnthor.h"
#include "gnthor/system_stats.h"
#include "gnthor/internal.h"
#include "gnthor/Reordering.h"

// offset of internal buffers taken from shared memory; this is based on the
// total number of nodes and set in sgasnet_attach
static size_t GNTHOR_SEGMENT_OFFSET = 0;

//
// replacement for gasnet_attach to call our initialization
//
int sgasnet_attach(gasnet_handlerentry_t *table, int numentries, uintptr_t segsize, uintptr_t minheapoffset)
{
    GNTHOR_TRACER(1);

// ask around for desired offset; right now, only coming in from system_stats; then
// align up to page boundary
    size_t desired_offset = gnthor::system_stats::estimate_segment_offset();
    GNTHOR_SEGMENT_OFFSET = (desired_offset/(size_t)GASNET_PAGESIZE+1)*GASNET_PAGESIZE;

    int result = gasnet_attach(table, numentries, segsize+GNTHOR_SEGMENT_OFFSET, minheapoffset);
    if (result == GASNET_OK)
        gnthor_initialize();
    return result;
}

//
// replacement for gasnet_getSegmentInfo to free up some internal space
//
int sgasnet_getSegmentInfo(gasnet_seginfo_t *seginfo_table, int numentries)
{
    GNTHOR_TRACER(1);
    int result = gasnet_getSegmentInfo(seginfo_table, numentries);

// offset the client-side numbers to make sure that it doesn't see the internal space
    if (result == GASNET_OK) {
        if (numentries > (int)gasnet_nodes()) numentries = (int)gasnet_nodes();
        for (int i = 0; i < numentries; ++i) {
            seginfo_table[i].addr = (char*)seginfo_table[i].addr+GNTHOR_SEGMENT_OFFSET;
            seginfo_table[i].size -= GNTHOR_SEGMENT_OFFSET;
        }
    }

    return result;
}

//
// replacement for gasnet's split barriers to allow Reordering analysis to run in overlap
//
static bool s_barrier_done = false;
void sgasnet_barrier_notify(int id, int flags)
{
    GNTHOR_TRACER(1);
    if (GNTHOR_ANALYSIS_IN_BARRIER && gnthor::internal::g_analysis_ready)
        s_barrier_done = gnthor::Reordering::get()->Analyze();
    assert(!internal::g_analysis_ready);

    if (!s_barrier_done)
        gasnet_barrier_notify(id, flags);
}

int sgasnet_barrier_wait(int id, int flags)
{
    GNTHOR_TRACER(1);
    if (!s_barrier_done)
        return gasnet_barrier_wait(id, flags);
    assert(GNTHOR_ANALYSIS_IN_BARRIER);
    s_barrier_done = false;
    return GASNET_OK;
}
