#ifndef __GNTHOR_PEERQUEUE_H__
#define __GNTHOR_PEERQUEUE_H__

#include <gasnet.h>
#include "gnthor/datatypes.h"

#include <algorithm>
#include <array>
#include <iostream>
#include <vector>
#include <assert.h>
#include <stddef.h>


namespace gnthor {

// queue of filed requests; for now, we use one queue per gasnet node
// so there is no domain mapping (and no domain_queue holding peers)
class PeerQueue {
public:
    typedef std::array<mem_req_t, REQ_QUEUE_LENGTH>::size_type size_type;

public:
    PeerQueue() {
    // m_head points to the one before the first unused slot (%length), m_tail_issued
    // to the last slot that was issued (%length; state == r_issued) and m_tail_completed
    // to the last completed request (%length) and is again potentially available
    //
    //  | ...              |
    //  | r_completed      |
    //  | ...              |
    //  | r_completed      |  <- m_tail_completed
    //  | r_issued         |
    //  | ...              |
    //  | r_issued         |  <- m_tail_issued
    //  | r_pending        |
    //  | ...              |
    //  | r_pending        |  <- m_head
    //  | 0 or r_completed |
    //
    // Numbers are only incremented: to get the slot, take %length. The
    // invariant that must hold is:
    //  m_tail_completed <= m_tail_issued <= m_head
    //
    // Note: the first slot will be skipped the first time around
        m_head = m_tail_issued = m_tail_completed = 0;
        m_requests[0].state = r_completed;
    }

public:
    gasnet_handle_t enqueue_request(req_type_t type,
        gasnet_node_t target, void* src, void* dest, size_t nbytes);

    int64_t add_request(gasnet_handle_t handle);

    mem_req_t& get_request(size_type index) {
        return m_requests[index];
    }

// Get a bundle of indices of messages ready to be issued; after this call,
// the messages are assumed to be unavailable for other servers.
    bool ready_for_issue() {
        return m_head - m_tail_issued != 0;
    }
    bool ready_for_issue(int bundle_size) {
        if (0 < bundle_size && (m_head - m_tail_issued < (size_type)bundle_size))
            return false;
        return ready_for_issue();
    }

    void get_bundle_for_issue(int bundle_size, std::vector<size_type>& bundle) {
        assert(ready_for_issue(bundle_size));     // insist on external check
        if (bundle_size < 0) {
            assert(m_tail_issued <= m_head);
            bundle_size = m_head - m_tail_issued; // i.e. all outstanding
        }
        assert(0 < bundle_size);
        bundle.resize(bundle_size);

        int rel_start = m_tail_issued + 1;
        m_tail_issued += bundle_size;
        assert(m_tail_completed <= m_tail_issued && m_tail_issued <= m_head);

        for (int ib = 0; ib < bundle_size; ++ib)
            bundle[ib] = (rel_start + ib) % REQ_QUEUE_LENGTH;

    // the queue assumes that the bundle is now issued, but the requests are
    // not marked as such; if the queue is not large enough, things will roll
    // over earlier (and should be handled in enqueue_request)
    }

// Get a bundle of indices of messages that have either been handed out for issue
// (and may be still pending) or are issued and ready to be waited on; after this
// call, the messages are assumed to be unavailable for other servers.
    void get_bundle_for_completion(int bundle_size, std::vector<size_type>& bundle) {
        if (0 < bundle_size) {
            bundle_size = std::min(m_head - m_tail_completed, (size_type)bundle_size);
        } else {
            bundle_size = m_head - m_tail_completed;
            bundle.resize(bundle_size);
        }

        int rel_start = m_tail_completed + 1;
        m_tail_completed += bundle_size;
        assert(m_tail_completed <= m_tail_issued && m_tail_issued <= m_head);

        for (int ib = 0; ib < bundle_size; ++ib)
            bundle[ib] = (rel_start + ib) % REQ_QUEUE_LENGTH;

    // the queue assumes that the bundle is now completed, but the requests are
    // not marked as such; if the queue is not large enough, things will roll
    // over earlier (and should be handled in enqueue_request)
    }

private:
    std::array<mem_req_t, REQ_QUEUE_LENGTH> m_requests;   // queue of outstanding requests

// TODO: currently no padding to prevent false sharing, as we don't
// support threads yet
    volatile size_type m_head;                    // one before available slot (%length)

    volatile size_type m_tail_issued;             // last issued request (%length)
    volatile size_type m_tail_completed;          // last completed request (%length)
};

//
// create a (incomplete) message request entry from an already posted message
//
inline int64_t PeerQueue::add_request(gasnet_handle_t handle)
{
// TODO: use atomic add if multiple threads can post to the queue
    size_type index = ++m_head % REQ_QUEUE_LENGTH;
    mem_req_t& mr = get_request(index);

// verify availability or wait as needed
    if (mr.state != r_completed) {
    // request buffer is full ...
        if (mr.state == r_pending) {
        // means the request is just sitting there: we need a bigger buffer!
        // TODO: don't think this is right r_pending <-> r_issued
            std::cerr << "peer request buffer is too small, increase REQ_QUEUE_LENGTH" << std::endl;
            std::terminate();
        } else if (mr.state == r_issued) {
        // in-flight, just wait for it to finish
        // TODO: look into nopoll
            gasnet_wait_syncnb(mr.handle);
        }
        // unknown state simply means first use (TODO: catch uninitialized)
    }

// save gasnet handle
    mr.handle = handle;
    mr.state  = handle == GASNET_INVALID_HANDLE ? r_completed : r_pending;
    // other entries irrelevant, as already pending
    return (int64_t)index;
}

} // namespace gnthor

#endif // ! __GNTHOR_PEERQUEUE_H__
