#ifndef __GNTHOR_SYSTEM_STATS_H__
#define __GNTHOR_SYSTEM_STATS_H__

#include <sys/types.h>

namespace gnthor {

namespace system_stats {
// stat's estimate of needed segment offset
    size_t estimate_segment_offset();
}

// to be called after setting up gasnet
int collect_latencies();

// target and source are both the remote connection
double put_injection(size_t target, size_t blocksize);
double put_latency(size_t target, size_t blocksize);
double put_estimate(size_t target1, size_t blocksize1, size_t target2, size_t blocksize2);

double get_injection(size_t source, size_t blocksize);
double get_latency(size_t source, size_t blocksize);
double get_estimate(size_t source1, size_t blocksize1, size_t source2, size_t blocksize2);

} // namespace gnthor

#endif // ! __GNTHOR_SYSTM_STATS_H__
