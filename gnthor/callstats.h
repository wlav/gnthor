#ifndef __GNTHOR_CALLSTATS_H__
#define __GNTHOR_CALLSTATS_H__

#include <stddef.h>
#include <stdint.h>

#define GNTHOR_COUNTER_FUNC(what)\
    void add_put_##what##_local()  { num_put_##what##_local  +=1; }\
    void add_put_##what##_remote() { num_put_##what##_remote +=1; }\
    void add_get_##what##_local()  { num_get_##what##_local  +=1; }\
    void add_get_##what##_remote() { num_get_##what##_remote +=1; }

#define GNTHOR_COUNTER(what)\
    uint64_t num_put_##what##_local;\
    uint64_t num_put_##what##_remote;\
    uint64_t num_get_##what##_local;\
    uint64_t num_get_##what##_remote;

#define GNTHOR_INITIALIZER(what)\
    num_put_##what##_local(0), num_put_##what##_remote(0),\
    num_get_##what##_local(0), num_get_##what##_remote(0)

namespace gnthor {

namespace internal {

class GNStats {
public:
    GNStats() : GNTHOR_INITIALIZER(nb), GNTHOR_INITIALIZER(nb_bulk),
                GNTHOR_INITIALIZER(nbi), GNTHOR_INITIALIZER(nbi_bulk),
                num_wait_syncnb(0), num_wait_syncnb_all(0),
                num_wait_syncnbi_gets(0), num_wait_syncnbi_puts(0), num_wait_syncnbi_all(0),
                num_try_syncnbi_gets(0), num_try_syncnbi_puts(0), num_try_syncnbi_all(0),
                num_bundle_issued(0), tot_bundle_size(0.)
    { }

    void print_stats(int node);

// gasnet-level stats
    GNTHOR_COUNTER_FUNC(nb)
    GNTHOR_COUNTER_FUNC(nb_bulk)
    GNTHOR_COUNTER_FUNC(nbi)
    GNTHOR_COUNTER_FUNC(nbi_bulk)

    void add_wait_syncnb()     { num_wait_syncnb     += 1; }
    void add_wait_syncnb_all() { num_wait_syncnb_all += 1; }

    void add_wait_syncnbi_gets() { num_wait_syncnbi_gets += 1; }
    void add_wait_syncnbi_puts() { num_wait_syncnbi_puts += 1; }
    void add_wait_syncnbi_all()  { num_wait_syncnbi_all  += 1; }
    void add_try_syncnbi_gets()  { num_try_syncnbi_gets  += 1; }
    void add_try_syncnbi_puts()  { num_try_syncnbi_puts  += 1; }
    void add_try_syncnbi_all()   { num_try_syncnbi_all   += 1; }

// gnthor-level stats
    void add_bundle_issued(size_t sz) { num_bundle_issued += 1; tot_bundle_size += sz; }

private:
    GNTHOR_COUNTER(nb)
    GNTHOR_COUNTER(nb_bulk)
    GNTHOR_COUNTER(nbi)
    GNTHOR_COUNTER(nbi_bulk)

    uint64_t num_wait_syncnb;
    uint64_t num_wait_syncnb_all;

    uint64_t num_wait_syncnbi_gets;
    uint64_t num_wait_syncnbi_puts;
    uint64_t num_wait_syncnbi_all;
    uint64_t num_try_syncnbi_gets;
    uint64_t num_try_syncnbi_puts;
    uint64_t num_try_syncnbi_all;

    uint64_t num_bundle_issued;
    double   tot_bundle_size;
};

} // namespace internal

extern internal::GNStats gnstats;

} // namespace gnthor

#endif // ! __GNTHOR_CALLSTATS_H__
