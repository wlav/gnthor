// File: gasnet_group_slow.cxx
// Author: Wim Lavrijsen (WLavrijsen@lbl.gov)

// The principle idea for reordering based on grouping is that some gasnet
// nodes will have less communication todo (e.g. the ones on the edges).
// That idea is then generalized to mean that some nodes have "less
// communication cost" for any reason, incl. being an edge node but also e.g.
// because they have shorter, faster, or simply less busy links.
//
// Scheme is as follows:
//   1) run a full exchange, measuring average time and dispersion
//   2) collect the timings from the exchange partners
//   3) sort the exchange partners based on their timings
//   4) re-execute based on the new orderings
//
// This code follows a simple 2D scheme like so (each number denotes a gasnet
// node):
//    0  1  2  3
//    4  5  6  7             N
//    8  9 10 11           W o E
//   12 13 14 15             S
//
// and the default exchange order is N-S-E-W.
//

#define MEASURE_PUT 1

static int ITERATIONS      = 10000;
static int EXPERIMENTS     = 10;
const bool VERBOSE         = true;

#include "common.h"


//
//--
//
int main( int argc, char* argv[] ) {
// see: https://bitbucket.org/berkeleylab/gasnet README for an example code of
// mixing MPI and gasnet

    WLAV_ENVVAL2(ITERATIONS);
    WLAV_ENVVAL2(EXPERIMENTS);

    WLAV_ENVVAL(BLOCKSIZE,        262144)

#include "setup.inc"

// collect measurements
    double exchange_time = 0., variance = 0.;
    double* durations = new double[ITERATIONS];

// measure totality for default exchange
    Partner* partners[] = DEFAULT_EXCHANGE_ORDER;
    for (int iexp = 0; iexp < EXPERIMENTS; ++iexp) {
        GASNET_BARRIER

        double exp_time = communicate_with(partners, data, true, durations);
        exchange_time += exp_time;
        exp_time /= ITERATIONS;
        for (int i = 0; i < ITERATIONS; ++i) {
            double d = durations[i] - exp_time;
            variance += d*d;
        }
        if (VERBOSE && !mynode) {
            std::cerr << mynode << ": (" << iexp << ") AVERAGE= " << exp_time
                      << ", STDDEV= " << sqrt(variance/ITERATIONS) << std::endl;
        }
    }
    exchange_time /= EXPERIMENTS*ITERATIONS;
    double stddev = sqrt(variance/(EXPERIMENTS*ITERATIONS));
    if (VERBOSE && !mynode)
        std::cerr << mynode << ": STD DEV: " << stddev << std::endl;

    GASNET_BARRIER

// let node zero report
    double buf[2];
    buf[0] = exchange_time; buf[1] = stddev;
    for (int inode = 0; inode < (int)gasnet_nodes(); ++inode) { // all nodes, for sorting partners later
        gasnet_put_nbi_bulk((gasnet_node_t)inode,
            (void*)((double*)segment_infos[inode].addr+2*mynode), buf, 2*sizeof(double));
    }
    gasnet_wait_syncnbi_puts();

    GASNET_BARRIER

    std::vector<double> all_def_times; all_def_times.reserve(gasnet_nodes());
    if (mynode == 0) {
        std::cerr << "default exchange times each gasnet node (average, stdev):\n";
        for (int inode = 0; inode < (int)gasnet_nodes(); ++inode) {
            double te = *((double*)segment_infos[0].addr+2*inode);
            double dt = *((double*)segment_infos[0].addr+2*inode+1);
            std::cerr << inode << ": " << te << " " << dt << std::endl;
            all_def_times.push_back(te);
        }
    } else {
        for (int inode = 0; inode < (int)gasnet_nodes(); ++inode) {
            double te = *((double*)segment_infos[mynode].addr+2*inode);
            all_def_times.push_back(te);
        }
    }

    std::vector<double> sorted_times = all_def_times;
    std::sort(sorted_times.begin(), sorted_times.end());
    double slowest_time_default = sorted_times.back();

// see where I fall
    auto iter = std::find(sorted_times.begin(), sorted_times.end(), exchange_time);
    size_t myindex = std::distance(sorted_times.begin(), iter);

// see who is slowest
    auto iter2 = std::find(all_def_times.begin(), all_def_times.end(), sorted_times.back());
    size_t slow_index = std::distance(all_def_times.begin(), iter2);

// do something with that knowledge ...

    GASNET_BARRIER

// now sort neigbors (high -> low in terms of workload) for slowest rank
    Partner* reordered_partners[] = {&south, &east, &west, &north, nullptr}; //DEFAULT_EXCHANGE_ORDER;
/*
    if (myindex == all_def_times.size()-1) {
        for (int i = 0; i < NUM_PARTNERS; ++i) { reordered_partners[i]->set_workload(segment_infos); }
        for (int i = 1; i < NUM_PARTNERS; ++i) {
            int j = i;
            while (j > 0 && reordered_partners[j-1]->workload < reordered_partners[j]->workload) {
                std::swap(reordered_partners[j-1], reordered_partners[j]);
                --j;
            }
        }
    } else {
    // if not the slowest, check whether reordering the slowest affects me
        for (int i = 0; i < NUM_PARTNERS; ++i) {
            if (reordered_partners[i]->node == slow_index) {
                if (VERBOSE) std::cerr << mynode << " considers reordering as a partner of \"slow\"\n";
            // see whether my communication with "slow" now overlaps with when "slow"
            // communicates with me, and if so, rotate myself out of the way
                Partner* slow_partners[NUM_PARTNERS];
                int sidx = (int)slow_index;
                int srow = (int)slow_index / irowsz;
                Partner* swest = new Partner{"west", gasnet_node_t((sidx-1) / irowsz < srow ? -1 : sidx-1), segment_infos};
                Partner* snorth = new Partner{"north", gasnet_node_t((sidx-irowsz) < 0 ? -1 : sidx-irowsz), segment_infos};
                Partner* seast = new Partner{"east", gasnet_node_t((sidx+1) / irowsz > srow ? -1 : sidx+1), segment_infos};
                Partner* ssouth = new Partner{"south", gasnet_node_t((sidx+irowsz) >= (int)gasnet_nodes() ? -1 : sidx+irowsz), segment_infos};
                slow_partners[0] = snorth;
                slow_partners[1] = swest;
                slow_partners[2] = ssouth;
                slow_partners[3] = seast;
                for (int i = 0; i < NUM_PARTNERS; ++i) { slow_partners[i]->set_workload(segment_infos); }
                for (int i = 1; i < NUM_PARTNERS; ++i) {
                    int j = i;
                    while (j > 0 && slow_partners[j-1]->workload < slow_partners[j]->workload) {
                        std::swap(reordered_partners[j-1], reordered_partners[j]);
                        std::swap(slow_partners[j-1], slow_partners[j]);
                        --j;
                    }
                }
            }
        }
    }
*/

    GASNET_BARRIER

// redo full exchange, check for improvements
    double reordered_time = 0.;
    for (int iexp = 0; iexp < EXPERIMENTS; ++iexp) {
        GASNET_BARRIER
        reordered_time += communicate_with(reordered_partners, data, true);
    }
    reordered_time /= EXPERIMENTS*ITERATIONS;

    GASNET_BARRIER;

// let node zero report (overwrite stdev)
    buf[0] = exchange_time; buf[1] = reordered_time;
    gasnet_put_bulk((gasnet_node_t)0,
        (void*)((double*)segment_infos[0].addr+2*mynode), buf, 2*sizeof(double));

    GASNET_BARRIER

    if (mynode == 0) {
        double slowest_time_reordered = 0.;
        std::cerr << "list of times for each gasnet node (default, reordered, reordered/default):\n";
        for (int inode = 0; inode < (int)gasnet_nodes(); ++inode) {
            double te = *((double*)segment_infos[0].addr+2*inode);
            double ts = *((double*)segment_infos[0].addr+2*inode+1);
            if (ts > slowest_time_reordered) slowest_time_reordered = ts;
            std::cerr << inode << ": " << te << " " << ts << " " << ts/te << std::endl;
        }
        std::cerr << "VERDICT (default, reordered, reordered/default): "
                  << slowest_time_default << " " << slowest_time_reordered << " "
                  << slowest_time_reordered/slowest_time_default << std::endl;
    }


    delete [] durations;

// done
#include "teardown.inc"

    return 0;
} 
