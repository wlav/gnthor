.PHONY: all clean

OPT?= dbg
MAK_VER?= seq
CONDUIT=udp

# expect GNTHOR_DIR pointing to gnthor, and GASNET_DIR pointing to the gasnet
# installation
GNTHOR_SRC=$(GNTHOR_DIR)
GNTHOR_INC=$(GNTHOR_SRC)

# set if any part of the code uses UPC
USES_UPCC?= no

ifeq ($(NERSC_HOST),cori)
   CONDUIT= aries
   CXX=CC
   MPICXX=CC
   EXTRA_CPPFLAGS += -DCORI_=1
else ifeq ($(NERSC_HOST),edison)
   CONDUIT= aries
   CXX=CC
   MPICXX=CC
   EXTRA_CPPFLAGS += -DEDISON_=1
else ifeq ($(NERSC_HOST), shepard)
   CONDUIT=mxm
   CXX=icpc
   CC=icc
   MPICXX=mpicxx
   EXTRA_CPPFLAGS += -DSHEPARD_=1
else ifeq ($(NERSC_HOST), cerebro)
   CONDUIT=udp
   CC=gcc
   CXX=g++
   MPICXX=mpicxx
   EXTRA_CPPFLAGS += -DCEREBRO_=1
endif

ifeq ($(USES_UPCC), yes)
      UPC_INST?=$(realpath $(dir $(shell which upcc))/..)
      include $(UPC_INST)/$(OPT)/include/$(CONDUIT)-conduit/$(CONDUIT)-$(MAK_VER).mak
      UPCC=$(UPC_INST)/$(OPT)/bin/upcc
      EXTRA_CPPFLAGS += -DGNTHOR_USES_UPCC
else
      include $(GASNET_DIR)/include/$(CONDUIT)-conduit/$(CONDUIT)-$(MAK_VER).mak
endif
EXTRA_CPPFLAGS += -std=c++14 -I$(GNTHOR_INC) $(GASNET_CPPFLAGS) -Wall -Wno-unused-function

#CCOPT= -fopenmp
ifeq ($(OPT),opt)
	CCOPT+= -O3 -DNDEBUG
else
       EXTRA_CPPFLAGS += -DGASNET_DEBUG -DGASNET_DEBUGMALLOC -DGASNET_STATS -DGASNET_TRACE
endif

# targets
GNTHOR_SRCS=$(wildcard $(GNTHOR_SRC)/*.cxx)
GNTHOR_OBJS=$(GNTHOR_SRCS:cxx=o)

GNTHOR_HEADERS = $(wildcard $(GNTHOR_SRC)/gnthor/*.h)

# build rules
all: $(GNTHOR_OBJS) $(GNTHOR_DIR)/libgnthor.a
%.o: %.cxx $(GNTHOR_HEADERS) $(GNTHOR_SRC)/gnthor.mak
	$(CXX) $(CCOPT) $(EXTRA_CPPFLAGS) $(GASNET_DEFINES) -c $*.cxx -o $*.o

$(GNTHOR_DIR)/libgnthor.a: $(GNTHOR_OBJS)
	ar rsv $(GNTHOR_DIR)/libgnthor.a $(GNTHOR_OBJS)

clean::
	rm -f $(GNTHOR_OBJS) libgnthor.a
