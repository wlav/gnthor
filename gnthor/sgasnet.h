#ifndef __GNTHOR_SGASNET_H__
#define __GNTHOR_SGASNET_H__

#include <gasnet.h>

#ifndef GNTHOR_INTERNAL

#warning GNTHOR is redefining gasnet APIs

#ifdef gasnet_attach
#undef gasnet_attach
#endif
#define gasnet_attach                   sgasnet_attach

#ifdef gasnet_getSegmentInfo
#undef gasnet_getSegmentInfo
#endif
#define gasnet_getSegmentInfo           sgasnet_getSegmentInfo

#ifdef gasnet_get_nb
#undef gasnet_get_nb
#endif
#define gasnet_get_nb                   sgasnet_get_nb

#ifdef gasnet_get_nb_bulk
#undef gasnet_get_nb_bulk
#endif
#define gasnet_get_nb_bulk              sgasnet_get_nb_bulk

#ifdef gasnet_put_nb
#undef gasnet_put_nb
#endif
#define gasnet_put_nb                   sgasnet_put_nb

#ifdef gasnet_put_nb_bulk
#undef gasnet_put_nb_bulk
#endif
#define gasnet_put_nb_bulk              sgasnet_put_nb_bulk

#ifdef gasnet_wait_syncnb
#undef gasnet_wait_syncnb
#endif
#define gasnet_wait_syncnb              sgasnet_wait_syncnb

#ifdef gasnet_wait_syncnb_all
#undef gasnet_wait_syncnb_all
#endif
#define gasnet_wait_syncnb_all          sgasnet_wait_syncnb_all

#ifdef gasnet_get_nbi
#undef gasnet_get_nbi
#endif
#define gasnet_get_nbi                  sgasnet_get_nbi

#ifdef gasnet_get_nbi_bulk
#undef gasnet_get_nbi_bulk
#endif
#define gasnet_get_nbi_bulk             sgasnet_get_nbi_bulk

#ifdef gasnet_put_nbi
#undef gasnet_put_nbi
#endif
#define gasnet_put_nbi                  sgasnet_put_nbi

#ifdef gasnet_put_nbi_bulk
#undef gasnet_put_nbi_bulk
#endif
#define gasnet_put_nbi_bulk             sgasnet_put_nbi_bulk

#ifdef gasnet_wait_syncnbi_gets
#undef gasnet_wait_syncnbi_gets
#endif
#define gasnet_wait_syncnbi_gets        sgasnet_wait_syncnbi_gets

#ifdef gasnet_wait_syncnbi_puts
#undef gasnet_wait_syncnbi_puts
#endif
#define gasnet_wait_syncnbi_puts        sgasnet_wait_syncnbi_puts

#ifdef gasnet_wait_syncnbi_all
#undef gasnet_wait_syncnbi_all
#endif
#define gasnet_wait_syncnbi_all         sgasnet_wait_syncnbi_all

#ifdef gasnet_try_syncnbi_gets
#undef gasnet_try_syncnbi_gets
#endif
#define gasnet_try_syncnbi_gets         sgasnet_try_syncnbi_gets

#ifdef gasnet_try_syncnbi_puts
#undef gasnet_try_syncnbi_puts
#endif
#define gasnet_try_syncnbi_puts         sgasnet_try_syncnbi_puts

#ifdef gasnet_try_syncnbi_all
#undef gasnet_try_syncnbi_all
#endif
#define gasnet_try_syncnbi_all          sgasnet_try_syncnbi_all

#ifdef gasnet_barrier_notify
#undef gasnet_barrier_notify
#endif
#define gasnet_barrier_notify           sgasnet_barrier_notify

#ifdef gasnet_barrier_wait
#undef gasnet_barrier_wait
#endif
#define gasnet_barrier_wait             sgasnet_barrier_wait

#endif

#ifdef __cplusplus
extern "C" {
#endif // ifdef __cplusplus

    int sgasnet_attach(gasnet_handlerentry_t *table, int numentries, uintptr_t segsize, uintptr_t minheapoffset);
    int sgasnet_getSegmentInfo(gasnet_seginfo_t *seginfo_table, int numentries);

    gasnet_handle_t sgasnet_get_nb(void *dest, gasnet_node_t node, void *src, size_t nbytes);
    gasnet_handle_t sgasnet_get_nb_bulk(void *dest, gasnet_node_t node, void *src, size_t nbytes);
    gasnet_handle_t sgasnet_put_nb(gasnet_node_t node, void *dest, void *src, size_t nbytes);
    gasnet_handle_t sgasnet_put_nb_bulk(gasnet_node_t node, void *dest, void *src, size_t nbytes);
    void sgasnet_wait_syncnb(gasnet_handle_t handle);
    void sgasnet_wait_syncnb_all(gasnet_handle_t *phandle, size_t numhandles);

    void sgasnet_get_nbi(void *dest, gasnet_node_t node, void *src, size_t nbytes);
    void sgasnet_get_nbi_bulk(void *dest, gasnet_node_t node, void *src, size_t nbytes);
    void sgasnet_put_nbi(gasnet_node_t node, void *dest, void *src, size_t nbytes);
    void sgasnet_put_nbi_bulk(gasnet_node_t node, void *dest, void *src, size_t nbytes);

    void sgasnet_wait_syncnbi_gets();
    void sgasnet_wait_syncnbi_puts();
    void sgasnet_wait_syncnbi_all();
    int sgasnet_try_syncnbi_gets();
    int sgasnet_try_syncnbi_puts();
    int sgasnet_try_syncnbi_all();

    void sgasnet_barrier_notify(int id, int flags);
    int sgasnet_barrier_wait(int id, int flags);

#ifdef __cplusplus
}
#endif // ifdef __cplusplus

#endif // !__GNTHOR_SGASNET_H__
