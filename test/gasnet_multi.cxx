#include "gasnet.h"
#include "gnthor/sgasnet.h"
#include "gnthor/gnthor.h"

#include <iostream>
#include <stdio.h>


#define GASNET_BARRIER \
   gasnet_barrier_notify(0, GASNET_BARRIERFLAG_ANONYMOUS);\
   gasnet_barrier_wait(0, GASNET_BARRIERFLAG_ANONYMOUS);

struct Partner {
   Partner(int offset, gasnet_seginfo_t* segment_infos) {
      rank  = (gasnet_mynode() + offset) % gasnet_nodes();
      segment = (char*)segment_infos[rank].addr;
   }
   int   rank;
   char* segment;
};

struct Data {
   Data(int s) : size(s) { posix_memalign(&data, 8, size); }
   ~Data() { free(data); }
   int size;
   void* data;

private:
   Data(const Data&) = delete;
   Data& operator=(const Data&) = delete;
};


int main(int argc, char **argv) {
    int nMessages = 1;

    std::cerr << "now running " << __FILE__ << " ... " << std::endl;
    gasnet_init(&argc, &argv);

    size_t segment_size = 64*1024*1024; // 64MB
    size_t segment_max = gasnet_getMaxGlobalSegmentSize();
    if (segment_size > segment_max) segment_size = segment_max;

    gasnet_attach(NULL, 0, segment_size, 0);

    int mynode = gasnet_mynode();

    if (argc == 2)
        nMessages = atoi(argv[1]);

// more config from environment
    const char* options[] = { "WLAV_TEST_PUT_NB",  "WLAV_TEST_GET_NB",
                              "WLAV_TEST_PUT_NBI", "WLAV_TEST_GET_NBI" };
    int selection = 0;
    for (auto opt : options) {
        const char* cscheme = getenv(opt);
        if (cscheme)
            break;
        selection += 1;
    }

    if (selection == sizeof(options)/sizeof(options[0]))
        selection = 0;

    if (mynode == 0)
        std::cerr << "will send: " << nMessages << " message" << (nMessages == 1 ? "" : "s")
                  << " using: " << options[selection]+10 << std::endl;

    int nnodes = gasnet_nodes();
    gasnet_seginfo_t* segment_infos = new gasnet_seginfo_t[nnodes];
    gasnet_getSegmentInfo(segment_infos, nnodes);
    Partner partner(nnodes/2, segment_infos);

    const int MESSAGE_SIZE = 128;
    Data d{MESSAGE_SIZE*nMessages};

    char* databuf = nullptr, *resultbuf = nullptr;
    switch(selection) {
    case 0:                             // TEST_PUT_NB(I)
    case 2:
        databuf   = (char*)d.data;
        resultbuf = (char*)segment_infos[mynode].addr;
        break;
    case 1:                             // TEST_GET_NB(I)
    case 3:
        databuf   = (char*)segment_infos[mynode].addr;
        resultbuf = (char*)d.data;
        break;
    default:
        std::cerr << "ERROR: [" << mynode << "] unknown selection: " << selection << std::endl;
        exit(4);
    }

    const char* frmt = "hello from %d";
    for (int i = 0; i < nMessages; ++i)
        snprintf(databuf+MESSAGE_SIZE*i, MESSAGE_SIZE, frmt, mynode);

    gasnet_handle_t* handles = new gasnet_handle_t[nMessages];
    size_t dlen = strlen(databuf);

    if (selection == 1 || selection == 3) {
	int nbDigitsMe = mynode / 10 + 1;
	int nbDigitsPartner = partner.rank / 10 + 1;
	if (nbDigitsMe < nbDigitsPartner)
	    dlen += nbDigitsPartner - nbDigitsMe;
    }

    GASNET_BARRIER;
    for (int i = 0; i < nMessages; ++i) {
        ptrdiff_t step = MESSAGE_SIZE*i;
        size_t msgsize = (dlen+2*i) < MESSAGE_SIZE ? (dlen+2*i) : MESSAGE_SIZE;
        switch(selection) {
        case 0:                         // TEST_PUT_NB
            handles[i] = gasnet_put_nb(partner.rank, partner.segment+step, (char*)d.data+step, msgsize);
            break;
        case 1:                         // TEST_GET_NB
            handles[i] = gasnet_get_nb((char*)d.data+step, partner.rank, partner.segment+step, msgsize);
            break;
        case 2:
            gasnet_put_nbi(partner.rank, partner.segment+step, (char*)d.data+step, msgsize);
            break;
        case 3:
            gasnet_get_nbi((char*)d.data+step, partner.rank, partner.segment+step, msgsize);
            break;
        default:
            std::cerr << "ERROR: [" << mynode << "] unknown selection: " << selection << std::endl;
            exit(4);
        }
    }

    switch(selection) {
    case 0:
    case 1:
        gasnet_wait_syncnb_all(handles, nMessages);
        break;
    case 2:
        gasnet_wait_syncnbi_puts();
        break;
    case 3:
        gasnet_wait_syncnbi_gets();
        break;
    default:
        std::cerr << "ERROR: [" << mynode << "] unknown selection: " << selection << std::endl;
        exit(4);
    }

    GASNET_BARRIER;

    char verify[MESSAGE_SIZE];
    snprintf((char*)verify, MESSAGE_SIZE, frmt, partner.rank);
    size_t vlen = strlen(verify);
    char* lastbuf = nullptr;
    for (int i = 0; i < nMessages; ++i) {
        ptrdiff_t step = MESSAGE_SIZE*i;
        if (strncmp(verify, resultbuf+step, vlen)) {
            lastbuf = resultbuf+step;
            break;
        }
    }

    if (lastbuf) {
        lastbuf[MESSAGE_SIZE-1] = '\0';
        std::cerr << "ERROR: [" << mynode << "] expected \"" << verify << "\" but got: \""
                  << lastbuf << "\" from [" << partner.rank << "]" << std::endl;
    } else {
        std::cerr << "[" << mynode << "] ... result verified okay" << std::endl;
    }

    delete [] handles;
    delete [] segment_infos;

    GASNET_BARRIER;
    // print_gnthor_stats(0);
    GASNET_BARRIER;
    gasnet_exit(0);
    return 0;
}
