#include "gnthor/config.h"

#define GNTHOR_INTERNAL 1
#include "gnthor/sgasnet.h"
#include "gnthor/debug.h"
#include "gnthor/datatypes.h"
#include "gnthor/internal.h"
#include "gnthor/callstats.h"

#define SYNC_BUFFER_SIZE 32


//
// sgasnet_get_nb
//
// Reimplements gasnet_get_nb by storing the request on the request queue for those
// gets that go off-node.
//
gasnet_handle_t sgasnet_get_nb(void* dest, gasnet_node_t node, void* src, size_t nbytes)
{
    if (!GNTHOR_CAPTURE_LOCAL && gnthor::internal::is_same_node(node)) {
        GNTHOR_TRACER(1);
        gnthor::gnstats.add_get_nb_local();
        gasnet_handle_t handle = gasnet_get_nb(dest, node, src, nbytes);
        if (handle != GASNET_INVALID_HANDLE) {
        // means that even though this request stayed on-node, the conduit decided to
        // post it, rather than completely immediately (such as a shared memory write)
            return gnthor::internal::internalize_handle(handle, true /* on-node */);
        }
        return handle;   // already finished or failure
    }

// TODO: implement splitting (?)
    GNTHOR_TRACER(1);
    gnthor::gnstats.add_get_nb_remote();
    return gnthor::internal::enqueue_request(q_memget, node, src, dest, nbytes);
}

//
// sgasnet_get_nb
//
// Same as non-bulk, just different underlying API and accounting.
//
gasnet_handle_t sgasnet_get_nb_bulk(void* dest, gasnet_node_t node, void* src, size_t nbytes)
{
    if (!GNTHOR_CAPTURE_LOCAL && gnthor::internal::is_same_node(node)) {
        GNTHOR_TRACER(1);
        gnthor::gnstats.add_get_nb_bulk_local();
        gasnet_handle_t handle = gasnet_get_nb_bulk(dest, node, src, nbytes);
        if (handle != GASNET_INVALID_HANDLE) {
        // means that even though this request stayed on-node, the conduit decided to
        // post it, rather than completely immediately (such as a shared memory write)
            return gnthor::internal::internalize_handle(handle, true /* on-node */);
        }
        return handle;   // already finished or failure
    }

// TODO: implement splitting (?)
    GNTHOR_TRACER(1);
    gnthor::gnstats.add_get_nb_bulk_remote();
    return gnthor::internal::enqueue_request(q_memget_bulk, node, src, dest, nbytes);
}


//
// sgasnet_put_nb
//
// Reimplements gasnet_put_nb by storing the request on the request queue for those
// puts that go off-node.
//
gasnet_handle_t sgasnet_put_nb(gasnet_node_t node, void* dest, void* src, size_t nbytes)
{
    if (!GNTHOR_CAPTURE_LOCAL && gnthor::internal::is_same_node(node)) {
        GNTHOR_TRACER(1);
        gnthor::gnstats.add_put_nb_local();
        gasnet_handle_t handle = gasnet_put_nb(node, dest, src, nbytes);
        if (handle != GASNET_INVALID_HANDLE) {
        // means that even though this request stayed on-node, the conduit decided to
        // post it, rather than completely immediately (such as a shared memory write)
            return gnthor::internal::internalize_handle(handle, true /* on-node */);
        }
        return handle;   // already finished or failure
    }

// TODO: implement splitting
    GNTHOR_TRACER(1);
    gnthor::gnstats.add_put_nb_remote();
    return gnthor::internal::enqueue_request(q_memput, node, src, dest, nbytes);
}

//
// sgasnet_put_nb_bulk
//
// Same as non-bulk, just different underlying API and accounting.
//
gasnet_handle_t sgasnet_put_nb_bulk(gasnet_node_t node, void* dest, void* src, size_t nbytes)
{
    if (!GNTHOR_CAPTURE_LOCAL && gnthor::internal::is_same_node(node)) {
        GNTHOR_TRACER(1);
        gnthor::gnstats.add_put_nb_bulk_local();
        gasnet_handle_t handle = gasnet_put_nb_bulk(node, dest, src, nbytes);
        if (handle != GASNET_INVALID_HANDLE) {
        // means that even though this request stayed on-node, the conduit decided to
        // post it, rather than completely immediately (such as a shared memory write)
            return gnthor::internal::internalize_handle(handle, true /* on-node */);
        }
        return handle;   // already finished or failure
    }

// TODO: implement splitting
    GNTHOR_TRACER(1);
    gnthor::gnstats.add_put_nb_bulk_remote();
    return gnthor::internal::enqueue_request(q_memput_bulk, node, src, dest, nbytes);
}


//
// sgasnet_wait_syncnb
//
// Reimplements gasnet_wait_syncnb to wait on completion for a single request from
// the queue. This version minimizes the number of polls.
//
void sgasnet_wait_syncnb(gasnet_handle_t handle)
{
    GNTHOR_TRACER(1);
    gnthor::gnstats.add_wait_syncnb();
    mem_req_t& mr = gnthor::internal::h2mr(handle);
    if (mr.state == r_completed)
        return;

// TODO: the following draining is for the benefit of global reordering, but not generally
// needed, so find something that works for all
    gnthor::internal::drain_pending_requests();
    assert(mr.state == r_issued || mr.state == r_completed /* latter b/c of drain*/);
    if (mr.state != r_completed) {
        if (GASNET_OK != gasnet_try_syncnb_nopoll(mr.handle))
            gasnet_wait_syncnb(mr.handle);
        mr.state = r_completed;
    }
}


//
// sgasnet_wait_syncnb_all
//
// Reimplements gasnet_wait_syncnb_all to wait on completion of all requests from
// the queue listed in the given array. This version minimizes the number of polls.
//
static void inline sync_buffers(
         gasnet_handle_t to_sync[], gasnet_handle_t to_complete[], size_t ntosync)
{
    gnthor::gnstats.add_wait_syncnb_all();
    gasnet_wait_syncnb_all(to_sync, ntosync);
    for (size_t i = 0; i < ntosync; ++i)
        gnthor::internal::h2mr(to_complete[i]).state = r_completed;
}

void sgasnet_wait_syncnb_all(gasnet_handle_t* phandle, size_t numhandles)
{
    GNTHOR_TRACER(1);
    gasnet_handle_t to_sync[SYNC_BUFFER_SIZE], to_complete[SYNC_BUFFER_SIZE];
    size_t ntosync = 0;
    for (size_t i = 0; i < numhandles; ++i) {
        gasnet_handle_t ihandle = phandle[i];
        if (ihandle == GASNET_INVALID_HANDLE)
            continue;
        mem_req_t& mr = gnthor::internal::h2mr(ihandle);
        phandle[i] = GASNET_INVALID_HANDLE;
        if (mr.state == r_completed)
            continue; // when completed internally due to too small request buffer
        else if (mr.state == r_pending) {
            gnthor::internal::drain_pending_requests();
            if (mr.state == r_completed)
                continue;     // draining may have caused completion
        }

        assert(mr.state == r_issued);
        if (GASNET_OK != gasnet_try_syncnb_nopoll(mr.handle)) {
            GNTHOR_TRACER(3);
        // can have dupes if was internally synced and slot reused (TODO: this is very inelegant :P )
            bool isdupe = false;
            for (size_t j = 0; j < ntosync; ++j) {
                if (to_sync[j] == mr.handle) {
                    isdupe = true;
                    break;
                }
            }
            if (!isdupe) {
                to_sync    [ntosync] = mr.handle;
                to_complete[ntosync] = ihandle;
                ntosync++;
            }

        // never allow more than SYNC_BUFFER_SIZE handles outstanding
            if (ntosync == SYNC_BUFFER_SIZE) {
                sync_buffers(to_sync, to_complete, ntosync);
                ntosync = 0;
            }
        } else {
            GNTHOR_TRACER(3);
            mr.state = r_completed;
        }
    }

    if (ntosync)
        sync_buffers(to_sync, to_complete, ntosync);
}


//
// sgasnet_get_nbi
//
// Reimplements gasnet_get_nbi by storing the request on the request queue for those
// gets that go off-node.
//
void sgasnet_get_nbi(void *dest, gasnet_node_t node, void *src, size_t nbytes)
{
    if (!GNTHOR_CAPTURE_LOCAL && gnthor::internal::is_same_node(node)) {
        GNTHOR_TRACER(1);
        gnthor::gnstats.add_get_nbi_local();
    // local is assumed to be fast: just don't bother with the overhead and
    // do a blocking get instead
        return gasnet_get_nbi(dest, node, src, nbytes);
    }

    GNTHOR_TRACER(1);
    gnthor::gnstats.add_get_nbi_remote();
    gnthor::internal::enqueue_request(q_memget_i, node, src, dest, nbytes);
}

//
// sgasnet_get_nbi_bulk
//
// Same as non-bulk, just different underlying API and accounting.
//
void sgasnet_get_nbi_bulk(void *dest, gasnet_node_t node, void *src, size_t nbytes)
{
    if (!GNTHOR_CAPTURE_LOCAL && gnthor::internal::is_same_node(node)) {
        GNTHOR_TRACER(1);
        gnthor::gnstats.add_get_nbi_bulk_local();
    // local is assumed to be fast: just don't bother with the overhead and
    // do a blocking get instead
        return gasnet_get_nbi_bulk(dest, node, src, nbytes);
    }

    GNTHOR_TRACER(1);
    gnthor::gnstats.add_get_nbi_bulk_remote();
    gnthor::internal::enqueue_request(q_memget_i_bulk, node, src, dest, nbytes);
}


//
// sgasnet_put_nbi
//
// Reimplements gasnet_put_nbi by storing the request on the request queue for those
// puts that go off-node.
//
void sgasnet_put_nbi(gasnet_node_t node, void* dest, void* src, size_t nbytes)
{
    if (!GNTHOR_CAPTURE_LOCAL && gnthor::internal::is_same_node(node)) {
        GNTHOR_TRACER(1);
        gnthor::gnstats.add_put_nbi_local();
    // local is assumed to be fast: just don't bother with the overhead and
    // do a blocking put instead
        return gasnet_put_nbi(node, dest, src, nbytes);
    }

    GNTHOR_TRACER(1);
    gnthor::gnstats.add_put_nbi_remote();
    gnthor::internal::enqueue_request(q_memput_i, node, src, dest, nbytes);
}

//
// sgasnet_put_nbi_bulk
//
// Same as non-bulk, just different underlying API and accounting.
//
void sgasnet_put_nbi_bulk(gasnet_node_t node, void* dest, void* src, size_t nbytes)
{
    if (!GNTHOR_CAPTURE_LOCAL && gnthor::internal::is_same_node(node)) {
        GNTHOR_TRACER(1);
        gnthor::gnstats.add_put_nbi_bulk_local();
    // local is assumed to be fast: just don't bother with the overhead and
    // do a blocking put instead
        return gasnet_put_nbi_bulk(node, dest, src, nbytes);
    }

    GNTHOR_TRACER(1);
    gnthor::gnstats.add_put_nbi_bulk_remote();
    gnthor::internal::enqueue_request(q_memput_i_bulk, node, src, dest, nbytes);
}

//
// Waits and try-waits for implicits; for now, make no difference between gets and
// puts. Simply drain the full queue on either wait. Nudge the reordering whenever
// a try is called.
//
void sgasnet_wait_syncnbi_gets()
{
    GNTHOR_TRACER(1);
    gnthor::gnstats.add_wait_syncnbi_gets();
    gnthor::internal::drain_pending_requests();
    gasnet_wait_syncnbi_gets();
}

void sgasnet_wait_syncnbi_puts()
{
    GNTHOR_TRACER(1);
    gnthor::gnstats.add_wait_syncnbi_puts();
    gnthor::internal::drain_pending_requests();
    gasnet_wait_syncnbi_puts();
}

void sgasnet_wait_syncnbi_all()
{
    GNTHOR_TRACER(1);
    gnthor::gnstats.add_wait_syncnbi_all();
    gnthor::internal::drain_pending_requests();
    gasnet_wait_syncnbi_all();
}

int sgasnet_try_syncnbi_gets()
{
    GNTHOR_TRACER(1);
    gnthor::gnstats.add_try_syncnbi_gets();
    assert(0); // the following does a full wait, not a try
    gnthor::internal::drain_pending_requests();
    return gasnet_try_syncnbi_gets();
}

int sgasnet_try_syncnbi_puts()
{
    GNTHOR_TRACER(1);
    gnthor::gnstats.add_try_syncnbi_puts();
    assert(0); // the following does a full wait, not a try
    gnthor::internal::drain_pending_requests();
    return gasnet_try_syncnbi_puts();
}

int sgasnet_try_syncnbi_all()
{
    GNTHOR_TRACER(1);
    gnthor::gnstats.add_try_syncnbi_all();
    assert(0); // the following does a full wait, not a try
    gnthor::internal::drain_pending_requests();
    return gasnet_try_syncnbi_all();
}
